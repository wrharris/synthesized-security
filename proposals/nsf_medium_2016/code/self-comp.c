// Take two copies of the parameters of agg.
void agg2(int x_0, int y_0, int x_1, int y_1) {
  // Run a copy of agg on the first copy of parameters.
  int r_0 = x_0 + y_0;
 L0:  if (r_0 <= 150) r_0 = -1;
 L1:  else r_0 = r_0 / 2;
  // Run a copy of agg on the second copy of parameters.
  int r_1 = x_1 + y_1;
 L2:  if (r_1 <= 150) r_1 = -1;
 L3:  else r_1 = r_1 / 2;
  return;
}
