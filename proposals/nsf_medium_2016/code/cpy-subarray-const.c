uint32 ct_eq(uint32 a, uint32 b) {
  uint32 c = a ^ b;
  uint32 d = ~c & (c - 1);
  return 0 - (d >> (sizeof(d) * 8 - 1)); }

void ct_copy_subarray(uint8 *out, const uint8 *in,
       uint32 len, uint32 l_idx, uint32 sub_len) {
  uint32 i, j;
  for (i = 0; i < sub_len; i++) out[i] = 0;
  for (i = 0; i < len; i++) {
    for (j = 0; j < sub_len; j++)
      out[j] |= in[i] & ct_eq(l_idx + j, i); } }
