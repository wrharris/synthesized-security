int vfs_rmdir(struct inode *dir,
              struct dentry *dentry) {
  may_delete(dir, dentry, 1);
  ...
  // remove the directory bound to dir
H1: selinux_inode_rmdir(dir, dentry);
H2: selinux_inode_permission(dir, MAY_WRITE);
  dir->i_op->rmdir(dir, dentry);
  ... }

