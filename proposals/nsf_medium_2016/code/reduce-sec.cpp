void reduce_sec(Channel<char*>& channel) {
  char key[KEY_SIZE];
  channel.recv(key, KEY_SIZE);
  char valuesBuf[VALUES_SIZE];
  channel.recv(valuesBuf, VALUES_SIZE);
  StringList *values = (StringList *) valuesBuf;
  long long usage = 0;
  for (char *value = values->begin();
       value != values->end();
       value = values->next()) {
    long lvalue = mystrtol(value, NULL, 10);
    usage += lvalue; }
  char cleartext[BUF_SIZE];
  sprintf(cleartext, "%s %lld", key, usage);
  channel.send(cleartext); }
