log_client:
C0: rt = ld_root();
C1: msg = ld(rt, MSG);
C2: write(msg, "new login attempted");
C3: log_gt := ld(rt, LOGGER);
    set_op_label(mem_label());
    set_op_declass(mem_declass());
C4: gate_call(log_gt);

mal_client:
M0: write(LOG, "no logins ever attempted");

