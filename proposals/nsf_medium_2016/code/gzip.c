gzip:
// Create RPC service for loop.
C0: s0 :=
  create_service(loop, mem_amb(), mem_caps());
C1: set_mod_service(loop, s0);
  jump loop;

/* Create input, output descriptors from 
 * the next filename. */
loop: 
  has_next := has_next_file();
  br has_next ? L3 : L6;
IN: in = open(IN, next_in_path());
OUT:out = open(OUT, next_out_path());
  /* Create a service with which to execute
   * the compression module. */
C2: s1 := create_service(cmp, no_amb(),
             rem(in, WR, rem(out, RD, mem_caps())));
C3: set_mod_service(cmp, s1);
  jump cmp;

