int permission(struct inode *inode, int mask) {
  ...
H0: selinux_inode_permission(dir, mask);
  inode->i_op->permission(inode,mask);
  ... }

int may_delete(struct inode *dir,
               struct dentry *vic, int isdir) {
  ...
  // DAC check for WRITE and EXEC permissions
  permission(dir, MAY_WRITE | MAY_EXEC);
  ... }
