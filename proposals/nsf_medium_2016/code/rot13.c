void rot13(char *buf, int sz) {
  char *buf1 = malloc((sz + 1) * sizeof(char));
  char a;
  while (a =~ *buf) {
    *buf1 = (~a-1 / (~((a | 32)) / 13 * 2 - 11) * 13);
    buf++;
    buf1++; }
  return buf1; }

