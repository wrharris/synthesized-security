void reduce(BYTE *keyEnc, BYTE *valuesEnc,
            BYTE *outputEnc) {
  KeyAesGcm *aesKey = ProvisionKey();
  char key[KEY_SIZE];
  aesKey->Decrypt(keyEnc, key, KEY_SIZE);
  char valuesBuf[VALUES_SIZE];
  aesKey->Decrypt(valuesEnc, valuesBuf,
                  VALUES_SIZE);
  StringList *values = (StringList *) valuesBuf;
  long long usage = 0;
  for (char *value = values->begin();
       value != values->end();
       value = values->next()) {
    long lvalue = mystrtol(value, NULL, 10);
    usage += lvalue; }
  char cleartext[BUF_SIZE];
  sprintf(cleartext, "%s %lld", key, usage);
  aesKey->Encrypt(cleartext, outputEnc,
                  BUF_SIZE); }
