log_init:
       rt = ld_root();
       /* Create category c to protect
        * the integrity of LOG */
LBL1a: c = create_cat();
LBL1b: set_op_label(upd_lv(mem_label(), c, LOW));
       // Create log as child of root.
L1:    create(rt, LOG);
       /* Create LOGGER gate that owns c
        * (owned by memory) */
LBL2a: set_op_declass(mem_declass());
L2:    creategate(rt, LOGGER, logger);
L3:    ret = ld(rt, RET);
       /* Return to the env unable to modify log
        * (higher than LOW at c, not owning c) */
LBL4a: set_op_label(upd_lv(mem_label(), c, MID));
LBL4b: set_op_declass(
         rem_declass_cat(mem_declass(), c));
L4:    gate_call(ret);

