logger:
       // append the message to LOG
L5:    msg = ld(rt, MSG);
L6:    txt = read(msg);
L7:    log = ld(rt, LOG);
L8:    append(log, txt);
L9:    ret = ld(rt, RET);
       // call the environment without ownership of c
LBL9a: set_op_label(mem_label());
LBL9b: set_op_declass(rem_declass_cat(mem_declass(),
                                      c));
L10:   gate_call(ret);

