% !TEX root = /Users/chenxiongqian/workspace/sim-itps/paper/p.tex
% Evaluation:
\section{Evaluation}
\label{sec:evaluation}
%
We performed an empirical evaluation of our approach to answer the
following questions:
%
\textbf{(1)} Can pairs of programs that perform non-trivial tasks and
use dissimilar control structure and independently-written program
logic and that are actually equivalent be proved equivalent with
independent equivalence proofs?
%
\textbf{(2)} Given such pairs of equivalent programs, can \sys infer
an independent equivalence proof for the programs efficiently?
%
\textbf{(3)} Given a pair of programs perform non-trivial tasks that
are actually not equivalent, can \sys find inputs that prove the
non-equivalence of the given programs?

% Summary of procedure:
The answer the above experimental questions, we implemented \sys as a
tool that attempts to verify the equivalence of programs represented
in JVM bytecode.
%
We applied \sys to attempt to prove equivalence of $34$ pairs of
programs posted as solutions by independent programmers to the coding
platform \leetcode, a popular platform for maintaining programming
challenge problems and potential solutions to the solutions submitted
by different programmers.

% Summarize results:
In short, our experiments answer the above questions positively: \sys
was able to prove the equivalence of an overwhelming majority of pairs
of equivalence programs that it checked, and was able to find
counterexamples of equivalence for all pairs of programs that we
believe to be not equivalent.
%
When \sys terminated, it always found a proof of equivalence in a few
minutes, and typically found one in well under a minute.
%
The results indicate that independent equivalence proofs are
sufficiently expressive to express proofs of equivalence for
challenging programming assignments, and that \sys can typically infer
such proofs with sufficient efficiency to be either a useful tool to
be used directly as an education aid, or as an underlying engine for
other educational aids~\cite{singh13}.

% Outline the rest of the section.
In the rest of this section, we describe our evaluation of \sys in
detail.
%
In \autoref{sec:exp-procedure}, we describe our experimental
procedure;
%
in \autoref{sec:exp-results}, we present our experimental results and
draw preliminary conclusions.

\subsection{Experimental Procedure}
%
\label{sec:exp-procedure}
%
In this section, we describe the architecture of our implementation
(\autoref{sec:implementation}), the sources of the benchmarks on which
we evaluated our implementation (\autoref{sec:benchmarks}), and our
testing environment (\autoref{sec:setup}).

\subsubsection{Implementation}
\label{sec:implementation}
\sys is implemented as a Java package built on top of the Soot
analysis infrastructure for Java modules~\cite{soot}.
%
\sys takes as input
%
\textbf{(1)} to programs $P_0$ and $P_1$, each represented as a
collection of JVM bytecode classes;
%
\textbf{(2)} a method in each program that should be treated as the
program's entry point.
%
If \sys determines that $P_0$ is equivalent to $P_1$, then \sys
outputs independent proofs of the invariants satisfies by $P_0$ and
$P_1$, represented as \dotty~\cite{dotty} graphs.
%
If \sys determines that $P_0$ is \emph{not} equivalent to $P_1$, then
\sys generates a pair of runs from $P_0$ and $P_1$ from a single
shared input that result in distinct outputs.
%
The information about the runs contains the state of each program
after executing each instruction in each run, although \sys could be
extended to provide only information about the runs queried by the
user.

% Code features.
\sys is implemented in 3,215 lines of code.
%
It uses Soot to parse the input Java bytcode classes and construct the
input programs' interprocedural control-flow graph, which it expands
to an annotated path trees, and uses the Z3 interpolating theorem
prover~\cite{z3} to synthesize tree interpolants when refuting pairs
of control paths.
%
We plan to release the source code for \sys under a free license.

\subsubsection{Benchmarks}
\label{sec:benchmarks}
%
We collected as benchmarks programs submitted as solutions to
problems posed on the coding platform \leetcode.
%
In total, we collected $34$ pairs of solutions submitted for $24$
problems.

\subsubsection{Testing Environment}
\label{sec:setup}
%
We ran \sys on each pair of benchmark programs and measured its
performance;
%
the measured performance features are contained in
\autoref{table:bench}, and are discussed in detail in
\autoref{sec:exp-results}.
%
Many problem statements contained only partial specifications, in that
they only place a requirement on a program's output for certain
inputs.
%
For such problems, we first attempted to verify that multiple
solutions were equivalent on all inputs, which is feasible in the case
that both programs respond identically to unspecified inputs.
%
If \sys determined that such a pair of programs was not equivalent,
then we inspected counterexamples provided by \sys to confirm that
they are inputs for which proper behavior was undefined in the
specification.
%
If so, we extended both programs with a \emph{filter} that first
checked that the input satisfied the conditions given in the problem
statement, and if not, immediately threw an exception.
%
We then checked the equivalence of the given solutions composed with
the input filter.

% How did we run it.
All data presented are the result of running \sys three times on each
benchmark and reporting the arithmetic mean.
%
Each experiment was run on a machine with four 2.3 GHz processors and
16 GB of RAM
%
We plan to release a container with a replication of our experimental
setup.

\subsection{Experimental Results}
\label{sec:exp-results}
% Include the results table:
\input{fig/bench.tex}
%
\autoref{table:bench} contains the results of our experiments.
%
Note that some problems, in particular MajorityElement, MoveZeroes,
RemoveDuplicates, and RemoveElement, had received more than two
solutions;
%
we thus include the result of checking the equivalence of multiple
pairs of solutions.
%
Note that solutions provided for the same problem typically have
significantly different control structure: comparing only the number
of conditional branches and loops in two programs is only an
approximate means of comparing their similarity, under which programs
with dissimilar structure may appear similar.
%
However, even by this metric, the overwhelming majority of programs
($28$ out of $34$) programs have different numbers of conditional
branches, and almost half ($16$ out of $34$) have different numbers of
loops.

% Analyze the results.
Of the $34$ pairs of solutions posted, \sys proved equivalence of
$19$, found counterexamples to equivalence in $14$, and failed to
terminate on one.
%
We now describe the counterexamples produced for a selection of the
non-equivalent pairs, and draw general conclusions on the conditions
under which \sys does not terminate in practice.

%Inequivalent programs
\subsubsection{Non-equivalent Programs}
%
The programs in each of the $14$ pairs of non-equivalent programs,
provided as solutions to $12$ problems, were believed to be
equivalent.
%
Thus, for each of the $14$ pairs of non-equivalent programs, we
manually inspected the counterexample to equivalent generated by \sys,
determined the general class of inputs on which the two programs are
not equivalent, wrote a filter function to immediately catch such
inputs, and checked the filtered programs.
%
We believe that the problem of synthesizing such filters automatically
from examples is an exciting direction for future work.
%
We now discuss selected pairs of non-equivalent programs in detail.

% ClimbingStairs:
\textbf{ClimbingStairs}
%
We applied \sys to the one pair of solutions, and it generated the
counterexample $-1$.
%
However, the problem specifies that the input will be only a
non-negative integer.
%
We added a filter that checked this assumption, and \sys proved that
the filtering versions of the solutions are equivalent.

% FindMinArray:
\textbf{FindMinArray} 
%
We applied \sys to the two provided solutions;
%
the following are indicative of the counterexamples that \sys
generated: $[ ]$, $[ 11293, 2437, 2436 ]$, and $[ -1084, 7508, -474,
2399]$.
%
However, the problem specifies that each input array should be the
result of pivoting a sorted array on an arbitrary element.
%
We wrote a suitable input filter for this property, but \sys failed to
prove that the resulting programs are equivalent.

% H-Index2
\textbf{H-Index2}
%
We applied \sys to the two provided solutions;
%
the following are typical instances of the counterexamples that it
generated: $[ 4, 3, 4, 4 ]$ and $[ 8368, -8854, 1798 ]$.
%
However, the problem specifies that an input should contain only
non-negative integers sorted in ascending order.
%
We wrote a filter that checks this property, but \sys failed to
terminate when analyzing the filtering solutions.

% HouseRobber:
\textbf{HouseRobber} 
%
We applied \sys to the pair of provided solutions and it generated the
counterexample $[ -1 ]$.
%
However, the problem specifies that each input array contains only
non-negative integers.
%
After we added a suitable input filter, \sys proved the programs
equivalent.

% JumpGame:
\textbf{JumpGame} 
%
We applied \sys to the solutions provided for this problem, and \sys
generated exactly one counterexample: $[ ]$.
%
However, the problem specifies that each input should be a non-empty
array of non-negative integers.
%
We wrote a suitable input filter, and \sys proved that the filtering
programs are equivalent.

% MajorityElement:
\textbf{MajorityElement} We applied \sys to the three pairs of
provided solutions, and used \sys to generate four to eight
counterexamples for each pair.
%
Typical examples include $[]$, $[8, 10]$, $[19, 22, 22, 21]$.
%
However, the problem specifies that each input array contains a
majority element (i.e., if an input array is of length $n$, then it
contains an element that appears more than $\lfloor n/2 \rfloor$ times.
%Therefore, the generated 
We wrote a filter that checked this property, but \sys failed to
converge when applied to the filtering solutions.

% MaxSubarray:
\textbf{MaxSubarray} 
%
We applied \sys to the two provided solutions to generate the
following counterexamples: $[]$ and $[ \cc{Int.MIN\_VALUE} ]$.
%
However, the problem specifies that each input array that should
contain at least one element distinct the minimum and maximum integer
values.
%
We wrote a suitable input filter for this property, and \sys proved
that the filtering programs are equivalent.

% PeakElement:
\begin{comment}
\textbf{PeakElement} We applied \sys to the two provided solutions;
%
the following are some of the counterexamples that it generated: $[
2282, 2282 ]$ and $[ 281, 281, 280, 281 ]$.
%
However, the problem specifies that in each input array $a$, adjacent
elements in the array are distinct (i.e., for $a$ of length $n$ and $0
\leq i < n$, $a[i] \neq a[i + 1]$), and a solution may return the
index of any peak element.
%
Thus, we both wrote a filter that checks that adjacent elements are
distinct, and relaxed our equivalence requirement to require that
solutions output indices whose elements in the input array are equal.
%
\sys proved that the solutions are equivalent under this definition.
\end{comment}

% PlusOne
\textbf{PlusOne} We applied \sys to generate multiple counterexamples
for the two provided solutions, including $[ ]$, $[ 21 ]$, and $[-2]$.
%
However, the informal problem definition implies that a solution can
assume that an input is an array of digits.
%
Thus, we wrote a filter that checks that each input is an array of
digits.
%
We applied \sys to each solution composed with a filter, and \sys
proved that the programs are equivalent.

% RangeSumQuery:
\textbf{RangeSumQuery} 
%
We applied \sys to the two provided solutions to generate four
counterexamples: $([ ], 8856, 8854)$, $([ 1796 ], 611, 1)$, $([ ], 0,
0)$, and $([ 2282 ], 0, 1)$.
%
However, the problem specifies that each input satisfies the following
three conditions:
%
(1) the first argument is an array of integers;
%
(2) the second and third arguments are array indices;
%
(3) the second argument should be less than or equal to the third
argument.
%
We wrote a suitable input filter, and \sys proved that the filtering
programs were equivalent.

% SearchInsertPosition.
\textbf{SearchInsertPosition}
%
We applied \sys to the two provided solutions to generate the
following counterexamples: $([ -6483, -7719, -7719 ], -7719)$,
$([1796, -1, -8856], 0)$.
%
However, the problem specifies that in each vector of input arguments,
the first argument is a sorted and the array contains no duplicate
entries.
%
We wrote a suitable input filter for this property, but \sys failed to
prove that the filtering programs are equivalent.

% UniquePaths:
\textbf{UniquePaths}
%
We applied \sys to the two provided solutions.
%
The following are instances of counterexamples that it generated: $(1,
0)$, $(2, 0)$, and $(3, 0)$.
%
However, the problem specifies that each input should consist of two
inputs greater than $0$.
%
We wrote a suitable input filter for this property and \sys proved the
filtering solutions to be equivalent.

% Summarize:
In summary, we extended each of the $14$ pairs of solutions with input
filters that were suitable to the implicit assumptions of their
problem statement.
%
After doing so, \sys proved that $7$ of the pairs of solutions to $7$
problems were equivalent and failed to reach a definite answer for $7$
solutions to $5$ of the problems.

% Analyze 
\subsubsection{Conclusions}
%
Our results indicate that \sys is an effective tool for proving the
equivalence of simple programs with dissimilar structure if their
computations can be summarized accurately in the logic used by \sys to
model program semantics, \auflia (see \autoref{sec:ex-approach}).
%
For all pairs of programs in on which \sys failed to converge, the
programs compared performed fairly complex computations that could not
be precisely expressed using \auflia which cannot accurately model
non-linear operations or universally-quantified facts.
%
Thus, although \sys is an effective technique for reasoning about
programs with dissimilar structure, our experience using it indicates
than an enhancement increases the flexibility of its proof system
would have significant practical benefit.
%
This experience motivates our proposed future work on inferring path
sensitive simulation relations (\autoref{sec:path-sim-rels}).
