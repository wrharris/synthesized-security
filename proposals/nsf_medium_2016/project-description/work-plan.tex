\section*{Collaborative Plan}
\label{sec:work-plan}
%
In this section, we provide a timeline along which we plan to
accomplish various subgoals of the project and discuss potential risks
to the success of the project and how they will be mitigated.

% timeline:
\subsection*{Timeline}
\label{sec:timeline}
% include a workflow diagram:
\begin{figure}[t]
  % timeline:
  \centering
  \includegraphics[width=0.55\columnwidth]{fig/syn-sec-work-plan.pdf}
  \caption{Timeline for performing the proposed work.}
  \label{fig:timeline}
\end{figure}
%
Our previous experience developing SyGuS (\autoref{sec:program-syn})
strongly indicates that synthesizer-generators are best developed by
iterating between phases in which ideas are refined both top-down---in
which high-level concepts influence concrete implementations---and
bottom-up---in which detailed technical issues shape high-level design
decisions.
%
Our work plan, a timeline for which is given in
\autoref{fig:timeline}, is strongly influenced by this previous
experience.
%
We plan to perform the proposed work over three years, organized as
follows.

% first year:
In the first year (\autoref{fig:timeline}, Col. Year 1), we will
abstract our experience in developing the secure program synthesizers
described in \autoref{sec:crit-instances} to develop an \emph{initial}
version of \sys.
%
We will instantiate the initial version to reimplement each of the
instrumeters developed using ad-hoc synthesis algorithms in previous
work as instances of a single meta-synthesis algorithm implemented in
\sys.

% second year:
In the second year (\autoref{fig:timeline}, Col. Year 2), each PI will
develop a synthesizer for one of the emerging programming problems
discussed in \autoref{sec:case-studies}.
%
Each PI will focus on a problem domain based on their area of prior
experience.
%
In particular, PI Harris will develop a synthesizer of DoS-free
services based on his prior experience in verifying complexity
bounds~\cite{srikanth17}.
%
co-PI Seshia will develop a synthesizer for correct SIR usage based on
his prior experience verifying SIR programs~\cite{sinha15,sinha16}.
%
co-PI Jha will develop a synthesizer for constant-time implementations
based on this prior experience on information-flow
control~\cite{harris10,nadkarni16}.

% year 3:
In the third year (\autoref{fig:timeline}, Col. Year 3), we will
develop a \emph{revised} version of \sys that integrates all
extensions of \sys developed over Year 2.
%
By integrating all such extensions, we can potentially develop a
synthesizer generator that can synthesize programs that are secure in
challenging combinations of contexts introduced independently in
previous work.
%
E.g., the revised version of \sys may generate a synthesizer that
generates SIRS that are free of timing-channels or DoS
vulnerabilities.

% coordination
Work performed at each of the institutions will be closely
coordinated.
%
The PI's and their students will coordinate a group kickoff meeting at
the beginning of the project to establish both long-term goals for the
project and mid-term goals for the upcoming year.
%
They will coordinate an in-depth project meeting once a year that will
coincide with the annual PI meetings hosted by the NSF for all
supported teams.
%
Between annual in-person meetings, they will hold weekly meetings as
video conferences and closely share research artifacts as they are
developed.

% risk management:
\subsection*{Risk management}
\label{sec:risk}
%
The proposed work would address multiple open and challenging problems
in both program synthesis and software security.
%
As a result, there are risks that not every original goal of the
project will be accomplished.
%
The following are known risks to the project's success and plans for
mitigating their negative effects.

% risk: can't develop good initial version
The team may not be able to develop an initial version of \sys that
generalizes previous instances of synthesis for security.
%
We believe that the risk of this outcome is low: at least one PI was
closely involved with the development of each of the individual
synthesizers discussed.
%
However, if we cannot develop a single framework that generalizes all
known synthesizers discussed, then we expect that we can split the
complete set of synthesizers into sets with similar features.
%
We will then develop a small set of synthesizer frameworks, one that
generalizes each group of synthesizers.

% cannot develop synthesizers for emerging problems:
The team may not be able to develop a synthesizer for some of the
emerging problems in secure programming.
%
We believe that the risk of this outcome is moderate: a significant
amount of novel work must be accomplished in each of these domains,
precisely because they have emerged only recently.
%
However, the PI's have developed extensive experience in verifying
properties related to the emerging problem
domains~\cite{sinha15,sinha16,srikanth17}.
%
If within some problem domain we cannot develop a synthesizer that
addresses the natural, complete version of the problem, then we will
develop a synthesizer that still provides significant utility by
allowing it to take limited \emph{advice} from an expert user,
e.g. templates that restrict the form of relational invariants for
which the synthesizer searches.

% no revised edition
The team may not be able to develop a revised version of \sys
that generalizes each of the synthesizers developed for emerging
synthesis problems.
%
We believe that the risk of this outcome is high: the outcome depends
both on findings from developing each of the synthesizers for
individual emerging synthesis problems developed in Year 2 and the
compatibility of features from each of the original problems.
%
If we cannot develop a single framework that generalizes synthesizers
for all emerging problems, then we will develop a set of frameworks,
each of which generalizes some subset of the emerging problems, along
with other novel problems discovered over the course of the project.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../p"
%%% End: 
