The enumerative learning algorithm~\cite{udupa-pldi13} adopts a dynamic programming
based search strategy that systematically enumerates concepts (expressions) 
in increasing order of complexity. Various complexity
metrics can be assigned to concepts, the simplest being the expression size.
The algorithm needs to store all enumerated expressions,
because expressions of a given size are composed to form larger
expressions in the spirit of dynamic programming. The algorithm
maintains a set of concrete test cases, obtained from the
counterexamples returned by the verification oracle. These concrete
test cases are used to reduce the number of expressions stored at each
step by the dynamic programming algorithm.

\begin{table}[t]
\vspace{-11pt}
\centering
\normalsize
\begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}cc}\tnl\hlx{hv}
Expression to Verifier & Learned Test Input\tnl\hlx{vhv}
$x$ & $\zug{x = 0, y = 1}$\tnl
$y$ & $\zug{x = 1, y = 0}$\tnl
$1$ & $\zug{x = 0, y = 0}$\tnl
$x + y$ & $\zug{x = 1, y = 1}$\tnl
$\ITE(x \leq y, y,x)$ & --\tnl\hlx{vh}
\end{tabular*}
\caption{A run of the enumerative algorithm}
\vspace{-10pt}
\label{tab:enumerative-alg-trace}
\end{table}

We demonstrate the working of the algorithm on the illustrative example.
Table~\ref{tab:enumerative-alg-trace} shows the expressions
submitted to the verification oracle (an SMT solver) during the
execution of the algorithm and the values for which the expression
produces incorrect results. Initially, the algorithm submits the
expression $x$ to the verifier. The verifier returns a counterexample
$\zug{x = 0, y = 1}$, corresponding to the case where the expression
$x$ violates the specification. The expression enumeration is started
from scratch every time a counterexample is added. 
All enumerated expressions are checked for conformance with the accumulated 
(counter)examples before making a potentially-expensive query to the verifier. 
%An expression is submitted to the verifier only when it behaves
%according to the specification on all the concrete examples obtained thus far.
In addition, suppose the
algorithm enumerates two expressions $e$ and $e'$ which evaluate to
the same value on the examples obtained so far, then only one of $e$ or
$e'$ needs to be considered for the purpose of constructing larger
expressions. 
%This is because the information present in the concrete
%test cases is insufficient to distinguish between $e$ and $e'$ and one
%of two things must be true: $(i)$ either $e$ and $e'$ are
%mathematically equivalent, in which case the optimization is sound,
%or, $(ii)$ the verification oracle must eventually return a
%counterexample which distinguishes $e$ and $e'$, thus ensuring the
%soundness of this optimization.

Proceeding with the illustrative example, the algorithm then submits
the expression $y$ and the constant $1$ to the verifier. The verifier
returns the values $\zug{x = 1, y = 0}$ and $\zug{x = 0, y = 0}$,
respectively, as counterexamples to these expressions. The algorithm
then submits the expression $x + y$ to the verifier. 
The verifier returns the values $\zug{x = 1, y = 1}$ as a
counterexample. The algorithm then
%submits no more incorrect expressions to the verifier. It eventually
submits the expression shown in the last row of
Table~\ref{tab:enumerative-alg-trace} to the verifier. The verifier
certifies it to be correct and the algorithm terminates.

The optimization of pruning based on concrete counterexamples 
helps in two ways. First, it reduces the number of
invocations of the verification oracle. In the example we have
described, the correct expression was examined after only four calls
to the SMT solver, although about $200$ expressions were enumerated by
the algorithm. Second, it reduces the search space for candidate expressions 
significantly (see~\cite{udupa-pldi13} for details). For
instance, in the run of the algorithm on the example, although the
algorithm enumerated about $200$ expressions, only about $80$
expressions were stored. 
%If at all the solution demanded a more
%complex expression, then this reduction could result in an
%exponentially smaller number of more complex expressions, the gains
%being cumulative at each level of the dynamic programming
%algorithm.

