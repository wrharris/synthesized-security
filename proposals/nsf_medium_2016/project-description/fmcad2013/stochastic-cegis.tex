The stochastic learning procedure is an adaptation of the algorithm
recently used by Schufza et al.~\cite{SOA13}
for program super-optimization. The learning algorithm of the CEGIS loop
uses the Metropolis-Hastings procedure to sample expressions.
The probability of choosing
an expression $e$ is proportional to a value $\mbox{Score}(e)$,
which indicates the extent to which $e$ meets the specification $\varphi$.
The Metropolis-Hastings algorithm guarantees that, in the limit, expressions
$e$ are sampled with probability proportional to $\mbox{Score}(e)$.
To complete the description of the search procedure, we need to define
$\mbox{Score}(e)$ and the Markov chain used for successor sampling.
%We consider the number of concrete examples that $e$ satisfies as
%a proxy for $\mbox{Score}(e)$. Specifically, we 
We define $\mbox{Score}(e)$ to be $\exp(-\beta C(e))$,
where $\beta$ is a smoothing constant (set by default to $0.5$),
and the cost function $C(e)$ is the number of concrete examples on
which $e$ does {\it not} satisfy $\varphi$.

We now describe the Markov chain underlying the search. Fix an expression
size $n$, and consider all expressions in $L$ with parse trees of
size $n$. The initial candidate is chosen uniformly at random from
this set~\cite{CFGRandomSelection}. Given a candidate $e$, we pick
a node $v$ in its parse tree uniformly at random. Let $e_{v}$ be
the subexpression rooted at this node. This subtree is replaced by
another subtree (of the same type) of size equal to $\left|e_{v}\right|$
chosen uniformly at random. Given the original candidate $e$, and
a mutation $e^{\prime}$ thus obtained, the probability of making
$e^{\prime}$ the new candidate is given by the Metropolis-Hastings
acceptance ratio $\alpha(e,e^{\prime})=\min(1,\mbox{Score}(e^{\prime})/\mbox{Score}(e))$.

The final step is to describe how the algorithm selects the expression size $n$.
Although the solver comes with an option to specify $n$, the expression size is
typically not known a priori given a specification $\varphi$.
Intuitively, we run concurrent searches for a range of values for
$n$. Starting with $n=1$, with some probability $p_{m}$ (set by
default to $0.01$), we switch at each step to one of the searches
at size $n\pm1$. If an answer $e$ exists, then the search at size
$n=\left|e\right|$ is guaranteed to converge.

Consider the earlier example for computing the maximum of two integers.
There are $768$ integer-valued expressions
in the grammar of size six. Thus, the probability of choosing $e=\ITE(x\leq0,y,x)$
as the initial candidate is $1/768$. The subexpression to mutate
is chosen uniformly at random, and so the probability of deciding
to mutate the boolean condition $x\leq0$ is $1/6$. Of the $48$ boolean
conditions in the grammar, $y\leq0$ may be chosen with probability
$1/48$. Thus, the mutation $e^{\prime}=\ITE(0\leq y,y,x)$ is considered
with probability $1/288$. Given a set of concrete examples $\{(-1,-4),(-1,-3),(-1,-2),(1,1),(1,2)\}$,
$\mbox{Score}(e)=\exp(-2\beta)$, and $\mbox{Score}(e^{\prime})=\exp(-3\beta)$,
and so $e^{\prime}$ becomes the new candidate with probability $\mbox{\ensuremath{\exp}}(-\beta)$.
If, on the other hand, $e^{\prime}=\ITE(x\leq y,y,x)$ had been the mutation
considered, then $\mbox{Score}(e^{\prime})=1$, and $e^{\prime}$
would have become the new candidate with probability $1$.


%\subsubsection*{Differences from Schufza et al \cite{SOA13}}
Our algorithm differs from that of Schufza et al.~\cite{SOA13} in three ways:
%There are three main differences from the algorithm used for super-optimization:
(1) we do not attempt to optimize the size of the expression while the
super-optimizer does so;
(2)  we synthesize expression graphs rather than straight-line assembly code,
and
(3) since we do not know the expression size $n$,
we run concurrent searches for different values of $n$,
whereas the super-optimizer can use the size of the input program as
an upper bound on program size.
%
%First, the super-optimization procedure runs in two phases: synthesis
%and optimization. Since at the end of the synthesis phase, we already
%have an expression $e$ that works, in our setting, we do not have
%to continue with the optimization phase. The drawback is that (when
%given a fixed $n$), unnecessarily large and typically unreadable
%answers are produced. Second, we are synthesizing expression trees
%rather than straight-line assembly code. Hence the process for considering
%successor candidates is more involved and requires us to sample productions
%from a CFG uniformly at random. Finally, since we do not know the
%expression size $n$, we need to run concurrent searches for different
%values of $n$. This is unnecessary in the synthesis phase of the
%super-optimizer since the input program places an upper bound on the
%program size.
