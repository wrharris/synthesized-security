
\begin{figure*}
\begin{tabular}{c}	
\includegraphics[bb = 51 231 740  380, width=2.0\columnwidth]{figures/bitvectorData.pdf} \\
\begin{tabular}{cc}
\includegraphics[bb = 50 89 740 524 , height=1.8in]{figures/integerData.pdf} &
\includegraphics[bb = 50 258 560 534, height=1.8in]{figures/booleanData.pdf} 
\end{tabular}
\end{tabular}
\caption{Selected performance results for the three classes of benchmarks}\figlabel{results}
\end{figure*}


We are in the process of assembling a benchmark suite of synthesis problems to provide a basis for side-by-side comparisons of different solution strategies. 
The current set of benchmarks is limited to synthesis of loop-free functions with no optimality criterion; nevertheless, the benchmarks provide an initial demonstration of the expressiveness of the base formalism and of the relative merits of the individual solution strategies presented earlier. Specifically, in this section we explore three key questions about the benchmarks and the prototype synthesizers.
%
\begin{itemize}
\item \textbf{Complexity of the benchmarks.} Our suite includes a range of benchmarks from simple toy problems to non-trivial functions that are difficult to derive by hand. Some of the benchmarks can be solved in a few hundredths of a second, whereas others could not be solved by any of our prototype implementations. In all cases, however, the complexity of the problems derives from the size of the space of possible functions and not from the complexity of checking whether a candidate solution is correct. 

\item \textbf{Relative merits of different solvers.} The use of a standard format allows us to perform the first side-to-side comparison of different approaches to synthesis. None of the implementations were engineered with high-performance in mind, so the exact solution times are not necessarily representative of the best that can be achieved by a particular approach. However, the order of magnitude of the solution times and the relative complexity of the different approaches on different benchmarks can give us an idea of the relative merits of each of the approaches described earlier. 

\item \textbf{Effect of problem encoding.} For many problems, there are different natural ways to encode the space of desired functions into a grammar, so we are interested in observing the effect of these differences in encoding for the different solvers.

\end{itemize}

To account for variability and for the constant factors introduced by the prototype nature of the implementations, we report only the order of magnitude of the solution times in five different buckets: $0.1$ for solution times less than half a second, $1$ for solution times between half a second and $15$ seconds, $100$ for solution times up to two minutes, $300$ for solution times of up to $5$ minutes, and infinity for runs that time out after $5$ minutes.  


The benchmarks themselves are grouped into three categories: hacker's delight problems, integer benchmarks, and assorted boolean and bit-vector problems. 

 
\subsubsection*{Hacker's delight benchmarks}
This set includes~57 different benchmarks derived from 20 different
bit-manipulation problems from the book \emph{Hacker's Delight}
~\cite{warren2002}. These bit-vector problems were among the first to
be successfully tackled by synthesis technology and remain an active
area of research~\cite{sketching:pldi05, sketching:asplos06,
loopfree-pldi11}. 
For these benchmarks, the goal is to discover clever implementations
of bit-vector transformations (colloquially known as
bit-twiddling). For most problems, there are three different levels of
grammars numbered $d0$, $d1$ and $d5$; level $d0$ involves only the
instructions necessary for the implementation, so the synthesizer only
needs to discover how to connect them together. Level $d5$, on the
other extreme, involves a highly unconstrained grammar, so the
synthesizer must discover which operators to use in addition to how to
connect them together.  

\figref{results} shows the performance of the three solvers on a
sample of the benchmarks. For the Hacker's Delight benchmarks (\C{hd})
we see that the enumerative solver dominates, followed by the
stochastic solver. The symbolic search was the slowest, failing to
terminate on 29 of the $57$ benchmarks. It is worth mentioning,
however, that none of the grammars for these problems required the
synthesizer to discover the bit-vector constants involved in the
efficient implementations. We have some evidence to suggest that the
symbolic solver can discover such constants from the full space of
$2^{32}$ possible constants with relatively little additional
effort. On the other hand, for many of these problems the magic
constants come from a handful of values such as $1$, $0$, or
\C{0xffffffff}, so it is unnecessary for the enumerative solver to
search the space of $2^{32}$ possible bit-vectors.  

Finally, because these benchmarks have different grammars for the same
problem, we can observe the effect of using more restrictive or less
restrictive grammars as part of the problem description. We can see in
the data that all solvers were affected by the encoding of the problem
for at least some benchmark; although in some cases, the pruning
strategies used by the solvers were able to ameliorate the impact of
the larger search space. 
 

\subsubsection*{Integer benchmarks}
These benchmarks are meant to be loosely representative of synthesis
problems involving functions with complex branching structures
involving linear integer arithmetic.  
One of the benchmarks is \C{array-search}, which synthesizes a 
loop-free function that finds the index of an element in a sorted
tuple of size $n$, for $n$ ranging from 2 to 16. This benchmark proved
to be quite complex, as no solver was able to synthesize this function
for $n>4$. The \C{max} benchmarks are similar except they compute the
maximum of a tuple of size $n$.  

\figref{results} shows the relative performance of the three solvers
on these benchmarks for sizes up to $4$. With one exception, the
enumerative solver is the fastest for this class of benchmarks,
followed by the stochastic solver. The exception was \C{max3} where
the stochastic solver was faster.  

\subsubsection*{Boolean/Bit-vector benchmarks}

The \C{parity} benchmark computes the parity of a set of Boolean
values. The different versions represent different grammars to
describe the set of Boolean functions. As with other benchmarks, the
enumerative solver was always faster, whereas the symbolic solver
failed on every instance. These results show the impact that different
encodings of the same space of functions can have on the solution time
for both of the solution strategies that succeeded. Unlike the \C{hd}
benchmarks where the different grammars for a given benchmark were
strict subsets of each other, in this case the encodings \C{AIG} and
\C{NAND} correspond to different representations of the same space of
functions. 

The \C{Morton} benchmarks, which involve the synthesis of a function
to compute Morton numbers, are intended as challenge problems, and
could not be completed by any of the synthesizers. 

\subsubsection*{Observations}
The number of benchmarks and the maturity of the solvers are too
limited to draw broad conclusions, but the overall trend we observe is
that the encoding of the problem space into grammar has a significant
impact on performance, although the solvers are often good at
mitigating the effect of larger search spaces. We can also see that
non-symbolic techniques can be effective in exploring spaces of
implementations and can surpass symbolic techniques, especially when
the problems do not require the synthesizer to derive complex
bit-vector constants, which is true for all the bit-vector benchmarks
used. Moreover, we observe that the enumerative technique was better
than the stochastic search for all but two benchmarks, so although
both implementations are immature, these results suggest that it may
be easier to derive good pruning rules for the explicit search than an
effective fitness function for the stochastic solver. 


The symbolic solver used for these experiments represents one of many
possible approaches to encoding the synthesis problem into a series of
constraints. We have some evidence that more optimized encodings can
make the symbolic approach more competitive, although there are still
many problems for which the enumerative approach is more
effective. Specifically, we have transcribed all the hacker's delight
and integer benchmarks into the input language of the Sketch synthesis
system~\cite{solar-phd08}. Sketch completed all but 11 of the \C{hd}
benchmarks, and it was able to synthesize \C{array-search} up to size
7. This experiment is not an entirely fair comparison because,
although Sketch uses a specialized constraint solver and carefully
tuned encodings, the symbolic solver presented in this paper uses a
direct encoding of the problem into sequences of constraints and uses
Z3, a widely used off-the-shelf SMT solver which is not as
aggressively tuned for synthesis problems. Despite these limitations,
the symbolic solver was able to solve many of the benchmarks,
providing a lower bound on what can be achieved with a straightforward
use of off-the-shelf technology.  

Moreover, the enumerative solver was able to solve more \C{hd}
problems than even the more optimized symbolic solver. The problems
where the enumerative solver succeeded but Sketch failed were the $d5$
versions of problems $11$, $12$, $14$ and $15$, which suggests that
the enumerative solver was better at pruning unnecessary instructions
from the grammar. On the other hand, the more optimized symbolic
solver did have a significant advantage in the \C{array-search}
benchmarks which the enumerative solver could only solve up to size 4. 

