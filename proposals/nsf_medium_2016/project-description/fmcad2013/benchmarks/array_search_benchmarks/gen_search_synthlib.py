import sys

# write the synthesis function findIdx
# takes the n array values x_1,...,x_n and k as input arguent
# returns the index i such that x_i < k < x_{i+1} 
def synthfun(n):
    s = "(synth-fun findIdx ("
    for i in range(1,n+1):
        s += " (y%d Int)"%i
    s += " (k1 Int)) Int "
    s += "((Start Int ("
    for i in range(n+1):
        s += " %d"%i
    for i in range(1,n+1):
        s += " y%d" %i
    s += " k1"
    s += " (ite BoolExpr Start Start)))"
    s += " (BoolExpr Bool ((< Start Start) (<= Start Start) (> Start Start) (>= Start Start)))))"
    return s

# return the string (findIdx x1 x2 ... xn k)
def fapp(n):
    s = "(findIdx"
    for i in range(1,n+1):
        s += " x%d"%i
    s += " k"
    s += ")"
    return s

# return the precondition (and (< x1 x2) (and (< x2 x3) (< x3 x4)))
def precond(n):
    def precond_rec(n,i):
        compstr = "(< x%d x%d)" % (i, i+1) 
        if i+1 == n:
            return compstr
        else:
            return "(and %s %s)" % (compstr, precond_rec(n,i+1)) 
    return precond_rec(n,1)

def main(argv):
    n = int(argv[0])
    print "(set-logic LIA)"
    
    # print the synthesis function grammar
    print synthfun(n)

    # declare the forall variables
    for i in range(1,n+1):
        print "(declare-var x%d Int)"%i
    print "(declare-var k Int)"

    fappstr = fapp(n)
    precondstr = precond(n)

    # print the constraints
    print "(constraint (=> %s (=> (< k x1) (= %s 0))))"% (precondstr,fappstr)
    print "(constraint (=> %s (=> (> k x%d) (= %s %d))))"% (precondstr,n,fappstr,n)
    for i in range(1,n):
        print "(constraint (=> %s (=> (and (> k x%d) (< k x%d)) (= %s %d))))" % (precondstr,i, i+1, fappstr, i)

    print "(check-synth)"

if (__name__=='__main__'):
    if len(sys.argv) != 2:
        print "Incorrect array size argument"
        print "USAGE: python gen_search_synthlib.py size_array"
        sys.exit(2)
    main(sys.argv[1:])
