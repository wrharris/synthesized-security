#define bvone {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}
#define bvzero {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
#define bvmax {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
#define bvonef {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1}
#define bvtwo {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}


int W=32;

bit[W] bvnot(bit[W] x){return !x;}
bit[W] bvneg(bit[W] x){return (!x)+1;}
bit[W] bvlshr(bit[W] x, bit[W] k) { return x >> ival(k);}
bit[W] bvashr(bit[W] x, bit[W] k) { bit[W] res = x >> ival(k); res[W-1] = x[W-1]; return res;}
bit[W] bvshl(bit[W] x, bit[W] k) { return x << ival(k);}
bit[W] bvand(bit[W] x,bit[W] y){return x&y;}
bit[W] bvor(bit[W] x,bit[W] y){return x|y;}
bit[W] bvxor(bit[W] x,bit[W] y){return x^y;}
bit[W] bvadd(bit[W] x, bit[W] y){return x + y;}
bit[W] bvsub(bit[W] x, bit[W] y){return x + bvneg(y);}
bit bvredor(bit[W] x){for(int i=0; i<W;i++) {if(x[i]==1) return 1;} return 0;}

bit[W] bvmul(bit[W] x, bit[W] y){
bit[W] res = 0;
for(int i=0; i<W;i++){
if(y[i] == 1) res = res + x << i;
}
return res;
}


bit[W] bvudiv(bit[W] x, bit[W] y){
  bit[W] q = 0;
  bit[W] r = 0;
  for(int i=W-1;i>=0;i--){
    r = r << 1;
    r[0] = x[i];
    if(bvule(y,r)){
      r = bvsub(r,y);
      q[i] = 1;
      }
    }
  return q;
}

bit[W] bvurem(bit[W] x, bit[W] y){
  bit[W] q = 0;
  bit[W] r = 0;
  for(int i=W-1;i>=0;i--){
    r = r << 1;
    r[0] = x[i];
    if(bvule(y,r)){
      r = bvsub(r,y);
      q[i] = 1;
      }
    }
  return r;
}


bit[W] bvsdiv(bit[W] x, bit[W] y){
return bvudiv(x,y);
}

bit[W] bvsrem(bit[W] x, bit[W] y){
return bvurem(x,y);
}


bit bvsle(bit[W] x, bit[W] y){
 if(x[0]>y[0]) return 1;
 if(x[0]<y[0]) return 0;
 if(x[0] == 0){
 for(int i=W-1; i>=1; i--){
   if(x[i]<y[i]) return 1;
   if(x[i]>y[i]) return 0;
 }
}
else{
 for(int i=W-1; i>=1; i--){
   if(x[i]<y[i]) return 0;
   if(x[i]>y[i]) return 1;
 }
}
 return 1;
}

bit bvslt(bit[W] x, bit[W] y){
 if(x[0]>y[0]) return 1;
 if(x[0]<y[0]) return 0;
 if(x[0] == 0){
 for(int i=W-1; i>=1; i--){
   if(x[i]<y[i]) return 1;
   if(x[i]>y[i]) return 0;
 }
}
else{
 for(int i=W-1; i>=1; i--){
   if(x[i]<y[i]) return 0;
   if(x[i]>y[i]) return 1;
 }
}
 return 0;
}


bit bvule(bit[W] x, bit[W] y){
if(x==y) return 1;
for(int i=W-1; i>=0; i--){
if(x[i]!=y[i]){
if(x[i]>y[i]) return 0;
else return 1;
}
}
}

bit bvult(bit[W] x, bit[W] y){
if(x==y) return 0;
 for(int i=W-1; i>0; i--){
   if(x[i]<y[i]) return 1;
   if(x[i]>y[i]) return 0;
 }
}


bit bvuge(bit[W] x, bit[W] y){
 for(int i=W-1; i>=0; i--){
   if(x[i]>y[i]) return 1;
   if(x[i]<y[i]) return 0;
 }
 return 1;
}

bit bvugt(bit[W] x, bit[W] y){
 for(int i=W-1; i>=0; i--){
   if(x[i]>y[i]) return 1;
   if(x[i]<y[i]) return 0;
 }
 return 0;
}


int ival(bit[W] x){
int k = 0;
//for(int i=4;i<W;i++) assert x[i] == 0;
for(int i=0; i<4;i++) k = k + 2*x[i]*i;
return k;
}

