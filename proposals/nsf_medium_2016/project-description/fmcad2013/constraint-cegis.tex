The symbolic CEGIS approach uses a constraint solver both for
searching for a candidate expression that works for a set of concrete
input examples (concept learning) and verification of validity of an
expression for all possible inputs. We use component based synthesis
of loop-free programs as described by Jha et al.~\cite{jha-icse10,loopfree-pldi11}. 
Each production in the grammar corresponds to a component in a library.
A loop-free program comprising these 
components corresponds to an expression from the grammar. 
Some sample components for the illustrative example are
shown in Table~\ref{tab:constraint-component-example} along with their
corresponding productions. 
\begin{table}[t]
\centering
\normalsize
\begin{tabular}{ l | l  l}
\hline
Production & Component\; \\
\hline
$E\rightarrow \ITE(B, E, E)$ & Inputs:& $(i_1:B) (i_2,i_3:E)$ \\
\;& Output:& $(o:E)$\\
\;& Spec:& $o=\ITE(i_1, i_2, i_3)$\\
\hline
$B\rightarrow E \leq E$ & Inputs:&$(i_1, i_2:E)$\\
\;&Output:& $(o:B)$\\
\;&Spec:& $o=i_1\leq i_2$\\
\hline
\end{tabular}
\caption{Components from Productions}
\label{tab:constraint-component-example}
\vspace{-10pt}
\end{table}

The input/output ports of these components are typed and only
well-typed programs correspond to well-formed expressions from the
grammar. To ensure this, Jha et al.'s encoding~\cite{loopfree-pldi11} is extended with
typing constraints. We illustrate the working of this
algorithm on the maximum of two integers example. The library of
allowed components is instantiated to contain one instance each of
$\ITE$ and all comparison operators($\leq ,\geq ,=$) and the concrete
example set is initialized with $\zug{x=0, y=0}$. The first candidate
loop-free program synthesized corresponds to the expression $x$. This
candidate is submitted to the verification oracle which returns with
$\zug{x=-1,y=0}$ 
as a counterexample. This counterexample is added to the concrete example set
and the learning algorithm is queried again. The SMT formula for
learning a candidate expression is solved in an incremental fashion; 
i.e., the constraint for every new
example is added to the list of constraints from the previous
examples.  
%The SMT solver used for solving learning constraints was Z3
%which supports incremental solving.
The steps of the algorithm on the illustrative example are shown in Table~\ref{tab:constraint-alg-trace}.

\begin{table}[b]
\medskip
\centering
\normalsize
\begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}c|l|c}\hlx{hv}
Iteration & Loop-free program & Learned counter-example\tnl\hlx{vhv}
1 & $o_1:=x$ & $\zug{x = -1, y = 0}$\tnl
\hline
2 & $o_1:=x~\leq~x$ & \;\tnl
\;&$o_2:=\ITE(o_1,~y,~x)$ & $\zug{x = 0, y = -1}$ \tnl
\hline
3&$o_1:=y~\geq~x$ & \;\tnl
\;&$o_2:=\ITE(o_1,~y,~x)$ &--\tnl\hlx{vh}
\end{tabular*}
\caption{A run of the constraint learning algorithm}
\label{tab:constraint-alg-trace}
\vspace{-10pt}
\end{table}

If synthesis fails for a component library, we add one instance of
every operator to the library and restart the algorithm with the new
library. 
%DEPENDS ON HOW SEC 4 DESCRIBES THE RESULTS: 
We also tried a
modification to the original algorithm~\cite{loopfree-pldi11},
in which, instead of searching for a loop-free program that utilizes all
components from the given library at once, we search for programs of
increasing length such that every line can still select any component
from the library. The program length is increased in an exponential
fashion (1, 2, 4, 8, $\cdots$) for a good coverage. This approach provides better
running times for most benchmarks in our set, but it can also be more
expensive in certain cases. 
