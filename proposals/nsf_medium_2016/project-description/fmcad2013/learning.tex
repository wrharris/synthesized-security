% !TEX root =  main.tex

%We start with the roots in inductive inference (specifically, active learning from examples),
%add the notion of querying oracles, and continue on to CEGIS.
%Inductive and deductive synthesis.  Many recent program synthesis techniques
%are combine inductive and deductive reasoning. 

Algorithmic approaches to program synthesis range over a wide spectrum, from
{\em deductive synthesis} to {\em inductive synthesis}.
%As mentioned in \secref{intro}, in
In deductive program synthesis (e.g.,~\cite{MannaWaldinger80}), a program is synthesized by 
constructively proving a theorem, employing logical inference and constraint solving.
On the other hand, inductive synthesis~\cite{gold-ic67,summers-jacm77,shapiro-book83} 
seeks to find a program matching a set of input-output examples. It is thus an instance of
learning from examples, also termed as {\em inductive inference} or {\em machine learning}~\cite{angluin-acmcs83,mitchell-97}.
Many current approaches to synthesis blend induction and deduction~\cite{seshia-dac12}; syntax guidance is
usually a key ingredient in these approaches.

Inductive synthesizers generalize from examples by searching a restricted space of programs. 
In machine learning, this restricted space is called the {\em concept class}, and each element of that
space is often called a candidate {\em concept}. 
The concept class is usually specified syntactically. 
Inductive learning is thus a natural fit for the syntax-guided synthesis problem
introduced in this paper: the concept class is simply the set $L$ of permissible expressions. 
%While syntax-guided deductive approaches also exist,
%such as template-based invariant synthesis (e.g.,~\cite{colon-cav03}), 
%they are trickier to implement for the richer grammar-based formalism of Sec.~\ref{problem}.
%
%In the rest of this section, we describe the connections between active learning
%and inductive synthesis, outline the counterexample-guided inductive synthesis approach,
%and present three instantiations of it.

\subsection{Synthesis via Active Learning}

A common approach to inductive synthesis is to formulate the overall synthesis problem 
as one of {\em active learning} using a {\em query-based} model. Active learning is a special
case of machine learning in which the learning algorithm can control the selection of examples that it generalizes
from and can query one or more oracles to obtain both examples as well as labels for those examples.
In our setting, we can consider the labels to be binary: positive or negative.  
A positive example is simply an interpretation to $f$ in the background theory $T$ that
is consistent with the specification $\varphi$; i.e.,
it is a valuation to the arguments of the function symbol $f$ along with the 
corresponding valuation of $f$ that satisfies $\varphi$. 
A negative example is any interpretation of $f$ that is not consistent with $\varphi$.
We refer the reader to a paper by Angluin~\cite{angluin-ml88} for an overview of various models
for query-based active learning. 

In program synthesis via active learning, the query oracles are often implemented using 
deductive procedures such as model checkers or satisfiability solvers. 
Thus, the overall synthesis algorithm usually comprises a top-level inductive learning algorithm 
that invokes deductive procedures (query oracles); e.g., in our problem setting, it is intuitive, although
not required, to implement an oracle using an SMT solver for the theory $T$.
Even though this approach combines induction and deduction, it 
is usually referred to in the literature simply as ``inductive synthesis.'' 
We will continue to use this terminology in the present paper.

Consider the syntax-guided synthesis problem of Sec.~\ref{problem}. Given the tuple ($T$, $f$, $\varphi$, $L$), 
there are two important choices one must make to fix an inductive synthesis algorithm:
(1) {\em search strategy:} How should one search the concept class $L$? and (2) 
{\em example selection strategy:} Which examples do we learn from?

\subsection{Counterexample-Guided Inductive Synthesis}

%Counterexample-guided inductive synthesis (commonly referred to by the acronym CEGIS)~\cite{sketching:asplos06,solar-phd08} 
Counterexample-guided inductive synthesis (CEGIS)~\cite{sketching:asplos06,solar-phd08} shown in Figure~\ref{fig:cegis}
is perhaps the most popular approach to inductive synthesis today. 
CEGIS has close connections to algorithmic debugging using counterexamples~\cite{shapiro-book83} and
counterexample-guided abstraction refinement (CEGAR)~\cite{CGJLV03}.
This connection is no surprise, because
both debugging and abstraction-refinement involve synthesis steps: synthesizing a repair in the former case, and
synthesizing an abstraction function in the latter (see~\cite{seshia-dac12} for a more detailed discussion).


The defining aspect of CEGIS is its example
selection strategy: {\em learning from counterexamples provided by a verification oracle}. 
The learning algorithm, which is initialized with a particular choice of concept class $L$ 
and possibly with an initial set of (positive) examples, proceeds by searching the space of candidate concepts
for one that is consistent with the examples seen so far. There may be several such consistent concepts,
and the search strategy determines the chosen candidate, an expression $e$. The concept $e$ is then
presented to the verification oracle $\VerifyOracle$, which checks the candidate against the correctness specification.
$\VerifyOracle$ can be implemented as an SMT solver that checks whether $\varphi[f/e]$ is valid modulo the theory $T$.
If the candidate is correct, the synthesizer terminates and outputs this candidate.
Otherwise, the verification oracle generates a counterexample, an interpretation to the symbols and free variables
in $\varphi[f/e]$ that falsifies it. This counterexample is returned to the learning algorithm, which
adds the counterexample to its set of examples and repeats its search; note that the precise encoding
of a counterexample and its use can vary depending on the details of the learning algorithm employed. 
It is possible that, after some number of iterations of this loop, 
the learning algorithm may be unable to find a candidate concept consistent
with its current set of (positive/negative) examples, in which case the learning step, and 
hence the overall CEGIS procedure, fails.
%Otherwise, the verification oracle $\VerifyOracle$ is invoked again, and the overall CEGIS loop repeats.

\begin{figure}[t]
\centering
\includegraphics[width=0.5\columnwidth]{figures/cegis-overview.pdf}
\caption{Counterexample-Guided Inductive Synthesis (CEGIS)}
\label{fig:cegis}
\end{figure}

Several search strategies are possible for learning a candidate expression in $L$, each with its 
pros and cons. In the following
sections, we describe three different search strategies and illustrate the main ideas in each
using a small example.

\subsection{Illustrative Example}

\input{bv-example}

\subsection{Enumerative Learning}
\input{enumerative-cegis}

\subsection{Constraint-based Learning}
\input{constraint-cegis}

\subsection{Learning by Stochastic Search}
\input{stochastic-cegis}


