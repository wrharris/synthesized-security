In {\em program verification}, we want to check if a program satisfies its logical specification.
%Verification technology has matured significantly in recent years with increasing
%industrial adoption~(see~\cite{DBLP:journals/cacm/BallLR11,DBLP:journals/cacm/BesseyBCCFHHKME10}).
Contemporary verification tools vary widely in terms of source languages, verification methodology,
and the degree of automation, but they all rely on repeatedly invoking an SMT (Satisfiability Modulo Theories)
solver. An SMT solver determines the truth of a given logical formula
built from typed variables, logical connectives, and typical operations such as arithmetic and 
array accesses (see~\cite{barrett-smtbookch09,DBLP:journals/cacm/MouraB11}).
Despite the computational intractability of these problems, modern SMT solvers are capable of
solving instances with thousands of variables due to sustained innovations in core algorithms,
data structures, decision heuristics, and performance tuning by exploiting the architecture of 
contemporary processors.
A key driving force for this progress has been the standardization of a common interchange format
for benchmarks called SMT-LIB (see smt-lib.org) and the associated annual competition (see smtcomp.org).
These efforts have proved to be instrumental in creating a virtuous feedback loop between 
developers and users of SMT solvers:
with the availability of open-source and highly optimized solvers, researchers from
verification and other application domains find it beneficial to translate their problems into the common format instead
of attempting to develop their own customized tools from scratch, and
the limitations of the current SMT tools are constantly  exposed by the ever growing repository of different kinds of benchmarks,
thereby spurring greater innovation for improving the solvers.



In {\em program synthesis}, we wish to automatically synthesize
an implementation for the program that satisfies the given correctness specification.
A mature synthesis technology has the potential of even greater impact on software quality than program
verification.
Classically, program synthesis is viewed as a problem in deductive theorem proving:
a program is derived from the constructive proof of the theorem that states that for all inputs,
there exists an output, such that the desired correctness specification holds
(see~\cite{MannaWaldinger80}).
Our work is motivated by a recent trend in synthesis in which the programmer, in addition to
the correctness specification, provides a syntactic template for the desired program.
For instance, in the programming approach advocated by the {\sc Sketch} system,
a programmer writes a partial program with incomplete details,
and the synthesizer fills in the missing details using
user-specified assertions as the correctness specification~\cite{sketching:pldi05}.
We call such an approach to synthesis {\em syntax-guided synthesis\/} (\sgs).
Besides program sketching, a number of recent efforts such as 
synthesis of loop-free programs~\cite{loopfree-pldi11},
synthesis of Excel macros from examples~\cite{gulwani12},
program de-obfuscation~\cite{jha-icse10},
synthesis of protocols from the skeleton and example behaviors~\cite{udupa-pldi13},
synthesis of loop-bodies from pre/post conditions~\cite{SGF10},
integration of constraint solvers in programming environments for program completion~\cite{KMPS12}, and
super-optimization by finding equivalent shorter loop bodies~\cite{SOA13},
all are arguably instances of syntax-guided synthesis.
Also related are techniques for automatic generation of invariants
using templates and by learning~\cite{colon-cav03,Rybalchenko10,SGHALN13}, and recent work 
on solving quantified Horn clauses~\cite{BMR13}.


Existing formalization of the SMT problem and the interchange format does not provide
a suitable abstraction for capturing the syntactic guidance.
The computational engines used by the various synthesis projects mentioned above rely
on a small set of algorithmic ideas, but have evolved independently with no mechanism for
comparison, benchmarking, and sharing of back-ends.
The main contribution of this paper is to define the {\em syntax-guided synthesis\/} (\sgs) problem
in a manner that (1) captures the computational essence of these recent proposals
and (2) is based on more canonical formal frameworks such as logics and grammars instead
of features of specific programming languages.
In our formalization, the correctness specification of the function $f$ to be synthesized
is given as a logical formula $\varphi$ that uses symbols from a background theory $T$.
The syntactic space of possible implementations for $f$ is described as a set $L$ of expressions
built from the theory $T$, and this set is specified using a grammar.
The syntax-guided synthesis problem then is to find an implementation expression $e\in L$
such that the formula $\varphi[f/e]$ is valid in the theory $T$.
To illustrate an application of the \sgs-problem, suppose we want to find a completion of
a partial program with holes so as to satisfy given assertions.
A typical \sgs-encoding of this task will translate the concrete parts of the partial program 
and the assertions into the specification formula $\varphi$,
while the holes will be represented with the unknown functions
to be synthesized,  and the space of expressions that can substitute the holes
will be captured by the grammar.  


Compared to the classical formulation of the synthesis problem that involves only the correctness specification,
the syntax-guided version has many potential benefits.
First, the user can use the candidate set $L$ to limit the search-space for
potential implementations, and this has significant computational benefits for solving the synthesis problem.
Second, this approach gives the programmer the flexibility to express the desired artifact using a combination 
of syntactic and semantic constraints. Such forms of {\em multi-modal\/} specifications have the potential to make programming
more intuitive.
Third, the set $L$ can be used to constrain the space of implementations for the purpose of performance optimizations.
For example, to optimize the computation of the product of two two-by-two matrices, we can limit the search
space to implementations that use only~7 multiplication operations, and such a restriction
can be expressed only syntactically.
Fourth, because the synthesis problem boils down to finding a correct expression 
from the syntactic space of expressions, this search problem lends itself to machine learning 
and inductive inference as discussed in Section~\ref{cegis}.
%and suggests solution strategies that are typically not used by constraint solvers.
Finally, it is worth noting that the statement ``there exists an expression $e$ in the language generated by
a context-free grammar $G$ such that the formula $\varphi[f/e]$ is valid in a theory $T$'' cannot be translated to
determining the truth of a formula in the theory $T$, even with additional quantifiers.
%Thus, theoretically, syntax-guided synthesis problems are not reducible to classical synthesis. However,
%solution strategies for solving $\exists\,\forall$-formulas employed by SMT solvers can inspire
%strategies for solving the syntax-guided synthesis problem.



The rest of the paper is organized in the following manner.
In Section~\ref{problem}, we formalize the core problem of syntax-guided synthesis
with examples. 
In Section~\ref{cegis}, we discuss a generic architecture for solving the proposed problem using
the iterative {\em counter-example guided inductive synthesis\/} strategy~\cite{sketching:asplos06}
that combines a learning algorithm with a verification oracle.
%a learning algorithm produces a candidate implementation $e\in L$, 
%and a verifier checks if the expression $e$ is correct (that is, if the formula $\varphi[f/e]$ is valid),
%and if not, produces a counter-example to be used by the learning algorithm in the next iteration.
%An existing SMT solver can naturally serve in the role of the verifier.
For the learning algorithm, we show how three techniques from recent literature can be adapted for our purpose:
the enumerative technique generates the candidate expressions of increasing size relying on the input-output
examples for pruning;
the symbolic technique encodes parse trees of increasing size using variables and constraints, and it calls an SMT solver to
find a parse tree consistent with all the examples encountered so far; and
the stochastic search uniformly samples the set $L$ of expressions as a starting point,
and then executes (probabilistic) traversal of the graph where
two expressions are neighbors if one can be obtained from the other by a single edit operation on the parse tree.
We report on a prototype implementation of these three algorithms, and evaluate their performance
on a number of benchmarks in Section~\ref{eval}.




