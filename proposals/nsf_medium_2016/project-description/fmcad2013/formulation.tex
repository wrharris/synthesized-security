At a high level, the functional synthesis problem consists of finding a function $f$ 
such that some logical formula $\varphi$ capturing the correctness of $f$ is valid. 
In syntax-guided synthesis, the synthesis problem is constrained in three ways: 
(1) the logical symbols and their interpretation are restricted to a \emph{background theory},
(2) the {\em specification\/} $\varphi$ is limited to a first order formula in the background theory   
with all its variables universally quantified, and 
(3) the universe of possible functions $f$ is restricted to 
syntactic expressions described by a \emph{grammar}. We now elaborate on each of these points.


\subsubsection*{Background Theory}
The syntax for writing specifications is the same as classical typed first-order logic,
but the formulas are evaluated with respect to a specified background theory $T$.
The theory gives the vocabulary used for constructing formulas, 
the set of values for each type,
and the interpretation for each of the function and relation (predicate) symbols in the vocabulary.
We are mainly interested in theories $T$ for which well-understood decision procedures are
available for determining satisfaction modulo $T$ (see~\cite{barrett-smtbookch09} for a survey).
A typical example is the theory of {\em linear integer arithmetic\/} (LIA)
where each variable is either a boolean or an integer, and the vocabulary consists of
boolean and integer constants, standard boolean connectives, addition ($+$), comparison ($\le$),
and conditionals (\ITE).
Note that
the background theory can be a combination of logical theories, for instance, LIA and the theory of uninterpreted functions with equality.


\subsubsection*{Correctness Specification}
For the function $f$ to be synthesized, we are given the type of $f$ and a formula $\varphi$
as its correctness specification. The formula $\varphi$ is a Boolean combination of 
predicates from the background theory, involving 
universally quantified free variables, symbols from the background theory, and the function symbol $f$, 
all used in a type-consistent manner. 
%For the rest of the paper, we will be expressing $\varphi$ in mathematical notation, 
%but the actual syntax of \syntlib{} is derived from \smtlib{} with only minimal extensions. 
%\asl{What is the right place to talk about syntax?}
\begin{example}
Assuming the background theory is LIA, consider the specification of a function $f$
of type $\int\times\int\mapsto\int$:
\[\varphi_1:\ f(x,y)=f(y,x)\ \wedge\ f(x,y)\ge x.\]
The free variables in the specification are assumed to be universally quantified:
a given function $f$ satisfies the above specification if the quantified formula $\forall x,y.\,\varphi_1$ holds,
or equivalently, if the formula $\varphi_1$ is valid.
\end{example}
%For the purpose of this paper, we can assume that the specification formula $\varphi$ itself is quantifier-free.
%As another example of a specification, consider the specification of a function $g$
%of type $\int\times\int\mapsto\int$:
%\[\varphi_2:\  g(0,0)=0\ \wedge\ g(1,1)=2.\]


\subsubsection*{Set of Candidate Expressions}
In order to make the synthesis problem tractable, the ``syntax-guided'' version allows the user to impose structural (syntactic) 
constraints on the set of possible functions $f$. The structural constraints are imposed by restricting $f$ to the set $L$ of 
functions defined by a given context-free grammar $G_L$. 
Each expression in $L$ has the same type as that of the function $f$,
and uses the symbols in the background theory $T$ along with the variables corresponding to the formal parameters of $f$. 

\begin{example}
Suppose the background theory is LIA, and the type of the function $f$ is 
$\int\times\int\mapsto\int$. We can restrict the set of expressions $f(x,y)$ to be linear expressions of the inputs 
by restricting the body of the function to expressions in the set $L_1$ described by the grammar below:
\[
\linterm\ :=\ x\mid y\mid\intconst\mid \linterm+\linterm
\]
Alternatively, we can restrict $f(x,y)$ to conditional expressions with no addition by restricting the body terms from the set $L_2$
described by:
\begin{eqnarray*}
\term & := & x\mid y\mid\intconst\mid \ITE(\cond,\term,\term)\\
\cond & := & \term\le\term\mid \cond\wedge\cond \mid \neg\cond\mid (\cond)
\end{eqnarray*}
\end{example}
Grammars can be conveniently used to express a wide range of constraints, and in particular, to bound the depth and/or the size of the desired expression.


\subsubsection*{\sgs\ Problem Definition}
Informally, given the correctness specification $\varphi$ and the set $L$ of candidates,
we want to find an expression $e\in L$ such that if we use $e$ as an implementation of the
function $f$, the specification $\varphi$ is valid.
Let us denote the result of replacing each occurrence of the function symbol $f$ in $\varphi$
with the expression $e$ by $\varphi[f/e]$.
Note that we need to take care of binding of input values during such a substitution:
if $f$ has two inputs that the expressions in $L$ refer to by the variable names $x$ and $y$,
then the occurrence $f(e_1,e_2)$ in the formula $\varphi$ must be replaced with the expression
$e[x/e_1,y/e_2]$ obtained by replacing $x$ and $y$ in $e$ by the expressions $e_1$ and $e_2$, respectively.
Now we can define the {\em syntax-guided synthesis} problem, \sgs\ for short, precisely:
\begin{quote}
Given a background theory $T$, 
a typed function symbol $f$,
a formula $\varphi$ over the vocabulary of $T$ along with $f$,
and a set $L$ of expressions over the vocabulary of $T$ and of the same type as $f$,
find an expression $e\in L$ such that the formula $\varphi[f/e]$ is valid modulo $T$.
\end{quote}
\begin{example}
For the specification $\varphi_1$ presented earlier, 
if the set of allowed implementations is $L_1$ as shown before,
there is no solution to the synthesis problem. On the other hand,
if the set of allowed implementations is $L_2$, a possible solution is the 
conditional if-then-else expression $\ITE(x\ge y,x,y)$.
\end{example}

In some special cases, it is possible to reduce the decision problem for syntax guided synthesis to the problem of deciding formulas in the background theory using additional quantification. For example, every expression in the set $L_1$ is equivalent to
$ax+by+c$, for integer constants $a,b,c$. If $\varphi$ is the correctness specification,
then deciding whether there exists an implementation for $f$ in the set $L_1$ corresponds
to checking whether the formula $\exists\,a,b,c.\,\forall\,X.\,\varphi[f/ax+by+c]$ holds,
where $X$ is the set of all free variables in $\varphi$.
%
This reduction was possible for $L_1$ because the set of all expressions in $L_1$ can be represented by a single parameterized expression in the original theory. However, the grammar may permit expressions of arbitrary depth which may not be representable in this way, as in the case of $L_2$.

%\subsection{Extensions}
%\label{sec:extended-probdef}
%In this section, we outline some key extensions motivated by our experience in building systems for program synthesis.


\subsubsection*{Synthesis of Multiple Functions}
A general synthesis problem can involve more than one unknown function. 
In principle, adding support for problems with more than one unknown function is merely a matter of syntactic sugar. 
For example, suppose we want to synthesize functions $f_1(x_1)$ and $f_2(x_2)$,
with corresponding candidate expressions given by grammars $G_1$ and $G_2$, with start non-terminals $S_1$ and $S_2$, 
respectively. Both functions can be encoded with a single function $f_{12}(\id,x_1,x_2)$.
The set of candidate expressions is described by the grammar that contains the rules of $G_1$ and $G_2$ along with a new
production $S\,:=\,\ITE(\id=0,S_1,S_2)$, with the new start non-terminal $S$.
Then, every occurrence of $f_1(x_1)$ in the specification can be replaced with $f_{12}(0, x_1,*)$ 
and every call to $f_2(x_2)$ can be replaced with $f_{12}(1, *, x_2)$. 
Although adding support for multiple functions does not fundamentally increase the expressiveness of the notation, 
it does offer significant convenience in encoding real-world synthesis problems.

\subsubsection*{Let Expressions in Grammar Productions}
The SMT-LIB interchange format for specifying constraints
allows the use of let expressions as part of the formulas, and this is supported
by our language also:
$(\nlet ~~ [\var=e_1] ~~ e_2)$.
%From a logical standpoint, $\nlet$ does not increase expressiveness.
%One can replace every occurrence of $\var$ in $e_2$, that is, replace the above \nlet-expression with
%$e_2[\var / e_1]$; or alternatively, one can make $\var$ a universally quantified variable and desugar 
%the above \nlet-expression into the constraint $(\var = e_1)\, \Rightarrow\, e_2$.
%The former substitution can lead to an exponential blowup in the size of the formulas,
%and the latter, on the other hand, loses information about the fact that $\var$ is not really 
%an independent variable, but is fully constrained by the value of $e_1$. 
%As we show in \secref{cegis}, this loss of information can be problematic for existing 
%synthesis algorithms and it is therefore important to preserve this information in the encoding of the problem.
While \nlet-expressions in a specification can be desugared, the same does not hold when they are used in a grammar.
As an example, consider the grammar below for the set of candidate expressions for the function $f(x,y)$:
\begin{eqnarray*}
T & := & (\nlet\ [z = U]\ z+z ) \\
U & := & x\mid y\mid\intconst\mid U+U\mid U*U\mid(U)
\end{eqnarray*}
The top-level expression specified by this grammar is the sum of two identical subexpressions built
using arithmetic operators, and such a structure cannot be specified using a standard context-free grammar.
In the example above, every $\nlet$ introduced by the grammar uses the same variable name. 
If the application of \nlet-expressions are nested in the derivation tree, 
the standard rules for shadowing of variable definitions determine which definition corresponds to which use of the variable. 
%There are cases, however, where it is useful to have \nlet-expressions introduce fresh variables; we use 
%the notation $(\nlet\ [\fresh=e1]\ e2)$ where $\fresh$ refers to a fresh variable name in scope. 
%Within the grammar choices, we can then use $\fresh$ to refer to one of the fresh variables introduced by an enclosing \nlet. 
%For example, the grammar below for the function $f(x,y)$
%captures the set of all expressions that can be formed using the operations of $+$ and $*$, and represented as 
%directed acyclic (expression) graphs (DAGs):
%\[T\ :=\ x\mid y\mid\intconst\mid T+T\mid T*T\mid(T)\mid(\nlet\ [\fresh=T]\ T)\mid\fresh\]
%Note that every expression generated by this grammar corresponds to a straight-line program using auxiliary 
%variables.
%
%\subsubsection*{Grammars with Constraints} 
%Consider a problem involving the following grammar for a function $f(x,y)$: 
%\[T \ := \  x \ \mid\   y\ \mid\ T / y.\]
%Using the correctness specification $\varphi$ for $f$,
%one can imposes constraints on the input-output behavior of the function, but in this case, 
%we would like to impose an additional requirement: the choice $x/y$ should only be considered 
%if the system can guarantee that the parameter $y$ will never equal zero. 
%
%In principle, this could be achieved by modifying the signature of the function so that in addition 
%to returning the desired value it also returns an error code indicating whether a division by zero took place. 
%In general, for any grammar that produces functions of type $G : D \rightarrow R$ we can modify 
%the grammar to generate a functions of type $G' : D \rightarrow (R, B)$, that in addition to 
%returning an expression also returns an error code. 
%We could then modify the specification $\varphi$ to also 
%check the error code for every use of the function. 
%However, this kind of conversion is only one option from a palette of
%possible methods to enforce the desired requirement; for example, an alternative 
%approach that enumerates candidate expressions could ensure that expressions 
%violating the requirement are never generated when the value of $y$ is known upfront.
%However, this kind of conversion is tricky and error prone. 
%We want a mechanism that allows us to specify the restriction the grammar so that 
%certain productions are only allowed if certain predicates are satisfied, without
%imposing a particular approach to enforce that restriction.
%
%This is achieved by annotating productions in the grammar with formulas from the background theory 
%that must hold if that production rule is used. 
%Using these grammars annotated by constraints, the original synthesis problem can be generalized as follows.
%Suppose that just like before, we have a specification $\varphi$ which involves a function $f(x)$ 
%which is required to be in the language $L$ of candidate expressions. Now, suppose the language is defined by 
%a grammar $G$ composed of a set of production rules $R_i$, where each production rule is annotated with a formula 
%$\psi_i(x)$. Then, for every expression $e \in L$ we can define a formula $\Psi_e(x)= \bigwedge_{i\in E} \psi_i(x)$ 
%where $E$ is the set of indices of all the production rules that were used to generate $e$ from the grammar $G$. 
%%
%The synthesis problem is then to find an expression $e \in L$ such that 
%\[\varphi[f/e]\ \wedge\  \bigwedge_{e'\in P_f}\ \Psi_e(e')\] 
%is valid, where $P_f$ is the set of all expressions that were passed as parameters to $f$ in the formula $\varphi$.%
%
%We can use these ``predicated productions'' to enforce the restriction on the grammar presented earlier as shown below. 
%\[T\ :=\  x \ \mid\   y\ \mid\ T / y\ \assm\ (y\not=0).\]
%The new grammar uses the syntax $\assm\ \psi$ to associate a constraint to a production rule, 
%in this case to require that $y \neq 0$ if the production with the right-hand-side $T/y$ is chosen.
%
%In presence of \nlet-expressions, the constraints can also refer to the auxiliary variables.
%The combination of these two features provides a powerful mechanism for specifying the desired set
%of expressions. For instance, one can specify that a particular arithmetic operation, say multiplication,
%be used only at most 5 times; or associate a constraint with the use of a production which is the conjunction
%of all the tests involved in conditionals along the path from the root to this specific application of the production.
%
%\subsection{Advanced idioms based on Extensions}
%\label{sec:adv-idioms}
%
%\subsubsection*{Counting with $let$ and grammar constraints}
%When combined with restrictions in the grammar, $let$ provides a powerful mechanism 
%for constraining the set of expressions that the synthesizer can consider. 
%
%For example, consider the grammar 
%\begin{eqnarray*}
%f(x, y) & := & T \\
%T & := & ~ x ~ | ~ y ~ | ~ T * T\\
%\end{eqnarray*}
%Now, suppose we want to restrict the grammar so that it only generates expressions with at most five multiplications.
%We can do this by combining let with constraints in the grammar as follows: 
%\begin{eqnarray*}
%f(x, y) & := & (let [lim=5] T) \\
%T & := & ~ x ~ | ~ y ~ | ~ \\
%&&
%\begin{array}{l}
%(let [c1 = N, c2=N]  \\
%~~ (let [v1=(let [lim=c1] T), \\
%~~~~~~~ v2=(let [lim=c2] T)] \\
%~~~~ (let [lim=lim-1-(c1+c2), \\
%~~~~~~~~~~val = v1*v2]~ Chk) \\
%~~  )  \\
%)
%\end{array}
%\\
%N & :=& 0 | 1 | 2 | 3 \\
%Chk &:= & val ~assuming~ lim > 0
%\end{eqnarray*}
%To make sense of the expressions above, it is useful to note that some of the production 
%rules for $T$ involve the variable $lim$, so we expect $T$ to always occur inside a let 
%expression that binds $lim$ to a particular value. Similarly, the terminal $Check$ 
%involves the variables $lim$ and $val$, so check can only be used in the context of 
%$let$ expressions that define these two variables. Note that when $T$ is first used 
%to define the body of $f$, $lim$ is bound to $5$. When $T$ appears recursively, 
%$lim$ is bound to $c1$ and $c2$ which correspond to the maximum number of multiplications 
%that can appear in each recursive use of $T$. The predicate in the Check production 
%rule is used to ensure that $c1+c2$ add up to less than the original budget $lim-1$. 
%
%\subsection{Path Conditions for predicates}
%
%Consider a problem with an unknown function of the form: 
%\begin{eqnarray*}
%f(x, y) & := & T \\
%T & := & \ITE(Cond, T, T) ~ | ~ x ~ | y ~ | ~ T / T \\
%Cond & := & \ldots
%\end{eqnarray*}
%
%Now, if we want to guarantee that the resulting code will never produce a division by zero, 
%we need to have a mechanism to ensure that the synthesizer only choses the production 
%$T \rightarrow T / T$ if it can ensure that the expression it generates for the denominator 
%can never be zero under the current context. 
%
\subsubsection*{SYNTH-LIB Input Format}
To specify the input to the \sgs\ problem, we have developed an
interchange format, called SYNTH-LIB, based on the syntax of
SMT-LIB2---the input format accepted by the SMT solvers (see
smt-lib.org).  The input for the \sgs\ problem to synthesize the
function $f$ with the specification $\varphi_1$ in the theory LIA,
with the grammar for the languages $L_1$ is encoded in SYNTH-LIB as:

\begin{quote}
\begin{small}
\begin{verbatim}
(set-logic LIA)
(synth-fun f ((x Int) (y Int)) Int
  ((Start Int (x y 
               (Constant Int) 
               (+ Start Start)))))
(declare-var a Int)
(declare-var b Int)
(constraint (= (f a b) (f b a)))
(constraint (>= (f a b) a))
(check-synth)
\end{verbatim}
\end{small}
\end{quote}
%The \texttt{set-logic} command brings the functions defined in the
%SMT-LIB2 theory LIA of Linear Integer Arithmetic into scope. The \texttt{synth-fun}
%command declares $f$ as a function to be synthesized, specifies its
%type ($int \times int \mapsto int$), and also specifies its structure
%using a grammar which generates integer valued expressions whose start
%non-terminal is $\texttt{Start}$. The \texttt{constraint} keyword
%specifies constraints on the function $f$ that must hold
%for \emph{all} values of the variables $a$ and $b$, which have been
%declared using the \texttt{declare-var} command. Finally,
%the \texttt{check-synth} command indicates that the specification is
%complete and instructs the solver to attempt to synthesize a
%definition for $f$ which is in the language $L_1$ and satisfies all
%the specified constraints.

\subsubsection*{Optimality Criterion}
The answer to our synthesis problem need not be unique:
there may be two expressions $e_1$ and $e_2$ in the set $L$ of allowed expressions
such that both implementations satisfy the correctness specification $\varphi$.
Ideally, we would like to associate a cost with each expression, and consider
the problem of {\em optimal synthesis\/} which requires the synthesis tool
to return the expression with the least cost among the correct ones.
A natural cost metric is the size of the expression.
In presence of \nlet-expressions, the size directly corresponds to the number
of instructions in the corresponding straight-line code, and thus such a metric
can be used effectively for applications such as super-optimization.
%If we wish to associate different costs with different operations, then one option
%is to use {\em weighted\/} grammars, where a cost is associated with each production, and the
%cost of the expression is the sum of the costs of all the productions used in its derivation.

