\documentclass[10pt,conference]{IEEEtran}
%\documentclass{ieee}


\input{macros}

\renewcommand{\baselinestretch}{0.97}
% audupa: tweaked to reduce the amount of ugly empty space after a float
\setlength{\textfloatsep}{1mm}

\begin{document}

\title{Syntax-Guided Synthesis}

\author{
  \makebox[25mm]{Rajeev Alur$^{\dag}$}
  \makebox[34mm]{Rastislav Bodik$^{\ddag}$}
  \makebox[37mm]{Garvit Juniwal$^{\ddag}$}
  \makebox[37mm]{Milo M. K. Martin$^{\dag}$}
  \makebox[34mm]{Mukund Raghothaman$^{\dag}$}\\
  \makebox[25mm]{Sanjit A. Seshia$^{\ddag}$}
  \makebox[34mm]{Rishabh Singh$^{\sharp}$}
  \makebox[37mm]{Armando Solar-Lezama$^{\sharp}$}
  \makebox[37mm]{Emina Torlak$^{\ddag}$}
  \makebox[34mm]{Abhishek Udupa$^{\dag}$}\\
  \makebox[50mm]{$^{\dag}$\small{University of Pennsylvania}}
  \makebox[60mm]{$^{\ddag}$\small{University of California, Berkeley}}
  \makebox[70mm]{$^{\sharp}$\small{Massachusetts Institute of Technology}}
}
%% \author{Rajeev Alur\\
%% University of Pennsylvania
%% \and
%% Rastislav Bodik\\
%% UC Berkeley
%% \and
%% Garvit Juniwal\\
%% UC Berkeley
%% \and
%% Milo M.K. Martin\\
%% University of Pennsylvania
%% \and 
%% Mukund Raghothaman\\
%% University of Pennsylvania
%% \and
%% Sanjit A. Seshia\\
%% UC Berkeley
%% \and
%% Rishabh Singh\\
%% MIT
%% \and
%% Armando Solar-Lezama\\
%% MIT
%% \and
%% Emina Torlak\\
%% UC Berkeley
%% \and
%% Abhishek Udupa\\
%% University of Pennsylvania
%% }

\maketitle
\thispagestyle{empty}



\begin{abstract}
The classical formulation of the program-synthesis problem is to find a program
that meets a correctness specification given as a logical formula.
Recent work on program synthesis and program optimization 
illustrates many potential benefits of allowing the user to supplement the logical
specification with a syntactic template that constrains the space of allowed implementations.
Our goal is to identify the core computational problem common to these proposals in a 
logical framework. 
%This effort is motivated by the fact that
%the SMT solvers play a critical role in contemporary tools for
%program analysis and verification, by providing a standard interchange format,
%repository of benchmarks, and common computational back-ends.
The input to the {\em syntax-guided synthesis\/} problem (\sgs)
consists of a background theory, a semantic correctness specification 
for the desired program given by a logical formula, and a syntactic set of candidate implementations
given by a grammar.
The computational problem then is to find an implementation from the set of candidate expressions
so that it satisfies the specification in the given theory.
We describe three different instantiations of the {\em counter-example-guided-inductive-synthesis\/} (CEGIS)
strategy for solving the synthesis problem, report on prototype implementations,
and present experimental results on an initial set of benchmarks.
\end{abstract}



\section{Introduction\seclabel{intro}}
\input{introduction}

\section{Problem Formulation\label{problem}}
\seclabel{problem}
\input{formulation}

\section{Inductive Synthesis\label{cegis}}
\seclabel{cegis}
%\section{Counterexample-Guided Inductive Synthesis\label{cegis}}
%\section{CEGIS Solution Strategies}

\input{learning}

\section{Benchmarks and Evaluation\label{eval}}
\input{evaluation}

\section{Conclusions\label{concl}}

Aimed at formulating the core computational problem common to many
recent tools for program synthesis in a canonical and logical manner,
we have formalized the problem of syntax-guided synthesis.
Our prototype implementation of the three approaches to solve this problem is the first
attempt to compare and contrast existing algorithms on a common set of benchmarks.
%We hope that this effort will facilitate independent development of better \sgs-solvers
%and high-level programming notations that use synthesis in a mutually beneficial manner.
We are already working on the next steps in this project. These consist of
(1) finalizing the input syntax (\syntlib) based on the input format of SMT-LIB2,
with an accompanying publicly available parser,
(2) building a more extensive and diverse repository of benchmarks, and
(3) organizing a competition for \sgs-solvers.
We welcome feedback and help from the community on all of these steps.


\subsubsection*{Acknowledgements}
We thank Nikolaj Bjorner and Stavros Tripakis for their feedback.
This research is supported by the NSF Expeditions in Computing project ExCAPE (award CCF 1138996).

%\bibliographystyle{IEEEtran}
%\bibliographystyle{short-abbrv}
%\bibliography{refs}

% Generated by IEEEtran.bst, version: 1.13 (2008/09/30)
\begin{thebibliography}{10}
\providecommand{\url}[1]{#1}
\csname url@samestyle\endcsname
\providecommand{\newblock}{\relax}
\providecommand{\bibinfo}[2]{#2}
\providecommand{\BIBentrySTDinterwordspacing}{\spaceskip=0pt\relax}
\providecommand{\BIBentryALTinterwordstretchfactor}{4}
\providecommand{\BIBentryALTinterwordspacing}{\spaceskip=\fontdimen2\font plus
\BIBentryALTinterwordstretchfactor\fontdimen3\font minus
  \fontdimen4\font\relax}
\providecommand{\BIBforeignlanguage}[2]{{%
\expandafter\ifx\csname l@#1\endcsname\relax
\typeout{** WARNING: IEEEtran.bst: No hyphenation pattern has been}%
\typeout{** loaded for the language `#1'. Using the pattern for}%
\typeout{** the default language instead.}%
\else
\language=\csname l@#1\endcsname
\fi
#2}}
\providecommand{\BIBdecl}{\relax}
\BIBdecl

\bibitem{barrett-smtbookch09}
C.~Barrett, R.~Sebastiani, S.~A. Seshia, and C.~Tinelli, ``Satisfiability
  modulo theories,'' in \emph{Handbook of Satisfiability},
    2009, vol.~4, ch.~8.

\bibitem{DBLP:journals/cacm/MouraB11}
L.~M. de~Moura and N.~Bj{\o}rner, ``Satisfiability modulo theories:
  {I}ntroduction and applications,'' \emph{Commun. ACM}, vol.~54, no.~9, 2011.

\bibitem{MannaWaldinger80}
Z.~Manna and R.~Waldinger, ``A deductive approach to program synthesis,''
  \emph{{ACM TOPLAS}}, vol.~2, no.~1, pp. 90--121, 1980.

\bibitem{sketching:pldi05}
A.~Solar-Lezama, R.~Rabbah, R.~Bod\'{\i}k, and K.~Ebcioglu, ``Programming by
  sketching for bit-streaming programs,'' in \emph{PLDI}, 2005.

\bibitem{loopfree-pldi11}
S.~Gulwani, S.~Jha, A.~Tiwari, and R.~Venkatesan, ``Synthesis of loop-free
  programs,'' \emph{SIGPLAN Not.}, vol.~46, pp. 62--73, June 2011.

\bibitem{GHS12}
S.~Gulwani, W.~R. Harris, and R.~Singh, ``Spreadsheet data manipulation using
  examples,'' \emph{Commun. ACM}, vol.~55, no.~8, pp. 97--105, 2012.

\bibitem{jha-icse10}
S.~Jha, S.~Gulwani, S.~A. Seshia, and A.~Tiwari, ``Oracle-guided
  component-based program synthesis,'' in \emph{ICSE}, 2010, pp. 215--224.

\bibitem{udupa-pldi13}
A.~Udupa, A.~Raghavan, J.~V. Deshmukh, S.~Mador-Haim, M.~M. Martin, and
  R.~Alur, ``{\textsc{Transit}: Specifying Protocols with Concolic Snippets},''
  in \emph{PLDI}, 2013, pp. 287--296.

\bibitem{SGF10}
S.~Srivastava, S.~Gulwani, and J.~S. Foster, ``From program verification to
  program synthesis,'' in \emph{POPL}, 2010, pp. 313--326.

\bibitem{KMPS12}
V.~Kuncak, M.~Mayer, R.~Piskac, and P.~Suter, ``Software synthesis
  procedures,'' \emph{Commun. ACM}, vol.~55, no.~2, pp. 103--111, 2012.

\bibitem{SOA13}
E.~Schkufza, R.~Sharma, and A.~Aiken, ``Stochastic superoptimization,'' in
  \emph{ASPLOS}, 2013, pp. 305--316.

\bibitem{colon-cav03}
M.~Col{\'o}n, S.~Sankaranarayanan, and H.~Sipma, ``Linear invariant generation
  using non-linear constraint solving,'' in \emph{CAV}, 2003, pp. 420--432.

\bibitem{Rybalchenko10}
A.~Rybalchenko, ``Constraint solving for program verification: Theory and
  practice by example,'' in \emph{CAV}, 2010, pp. 57--71.

\bibitem{SGHALN13}
R.~Sharma, S.~Gupta, B.~Hariharan, A.~Aiken, P.~Liang, and A.~V. Nori, ``A data
  driven approach for algebraic loop invariants,'' in \emph{ESOP}, 2013, pp.
  574--592.

\bibitem{BMR13}
N.~Bj{\o}rner, K.~L. Mc{M}illan, and A.~Rybalchenko, ``On solving universally
  quantified {H}orn clauses,'' in \emph{SAS}, 2013, pp. 105--125.

\bibitem{sketching:asplos06}
A.~Solar-Lezama, L.~Tancau, R.~Bod\'{\i}k, S.~A. Seshia, and V.~Saraswat,
  ``Combinatorial sketching for finite programs,'' in \emph{ASPLOS}, 2006.

\bibitem{gold-ic67}
E.~M. Gold, ``Language identification in the limit,'' \emph{Information and
  Control}, vol.~10, no.~5, pp. 447--474, 1967.

\bibitem{summers-jacm77}
P.~D. Summers, ``A methodology for {LISP} program construction from examples,''
  \emph{J. ACM}, vol.~24, no.~1, pp. 161--175, 1977.

\bibitem{shapiro-book83}
E.~Y. Shapiro, \emph{Algorithmic Program Debugging}.\hskip 1em plus 0.5em minus
  0.4em\relax Cambridge, MA, USA: MIT Press, 1983.

\bibitem{angluin-acmcs83}
D.~Angluin and C.~H. Smith, ``Inductive inference: Theory and methods,''
  \emph{ACM Computing Surveys}, vol.~15, pp. 237--269, Sep. 1983.

\bibitem{mitchell-97}
T.~M. Mitchell, \emph{Machine Learning}.\hskip 1em plus 0.5em minus 0.4em\relax
  McGraw-Hill, 1997.

\bibitem{seshia-dac12}
S.~A. Seshia, ``Sciduction: {C}ombining induction, deduction, and structure for
  verification and synthesis,'' in \emph{DAC},
2012, pp. 356--365.

\bibitem{angluin-ml88}
D.~Angluin, ``Queries and concept learning,'' \emph{Machine Learning}, vol.~2,
  pp. 319--342, 1988.

\bibitem{solar-phd08}
A.~Solar-Lezama, ``Program synthesis by sketching,'' Ph.D. dissertation,
  University of California, Berkeley, 2008.

\bibitem{CGJLV03}
E.~M. Clarke, O.~Grumberg, S.~Jha, Y.~Lu, and H.~Veith, ``Counterexample-guided
  abstraction refinement for symbolic model checking,'' \emph{J. ACM}, vol.~50,
  no.~5, pp. 752--794, 2003.

\bibitem{CFGRandomSelection}
B.~McKenzie, ``Generating strings at random from a context free grammar,''
  1997.

\bibitem{warren2002}
H.~S. Warren, \emph{Hacker's Delight}.\hskip 1em plus 0.5em minus 0.4em\relax
  Boston, MA, USA: Addison-Wesley Longman Publishing Co., Inc., 2002.

\end{thebibliography}



\end{document}
