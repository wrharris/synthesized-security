% Overview: illustrate on a running example.
\section{Overview}
\label{sec:overview}
%
\newcommand{\movezeroes}{\cc{MoveZeroes}\xspace}
%
In this section, we present the equivalence checker that we have
developed in previous work, \sys, by example.
%
In \autoref{sec:running-ex}, we introduce as a running example a pair
of alternative solutions to a programming problem chosen the popular
\leetcode platform for hosting code problems.
%
In \autoref{sec:ex-current}, we describe the limitations of current
equivalence checker when applied to attempt to prove the equivalence
of the programs in our running example.
%
In \autoref{sec:ex-approach}, we illustrate the key observation behind
how \sys successfully verifies that the two example programs are
equivalent.

\subsection{Two Dissimilar but Equivalent Programs}
\label{sec:running-ex}

% Code for running examples:
\begin{figure}
  \centering
  \begin{minipage}{0.48\linewidth}
    \input{code/ex1.java}    
  \end{minipage}
  %
  \begin{minipage}{0.48\linewidth}
    \input{code/ex0.java}    
  \end{minipage}
  %
  \caption{\movezeroes0 and \movezeroes1: two solutions provided for
    the \movezeroes problem hosted on \leetcode.
    %
    The problem statement for \movezeroes given on \leetcode
    is:
    %
    ``Given an array \cc{nums}, write a function to move all 0's to the
    end of it while maintaining the relative order of the non-zero
    elements.''
    % 
  }
  \label{fig:movezeroes-code}
\end{figure}
% Define the MoveZeroes problem.
The programming problem \movezeroes, posed on the popular \leetcode
platform for hosting coding problems, is the following: given an array
of integers stored in variable \cc{nums}, write a function that moves
each $0$ to the end, while maintaining the relative order of non-zero
elements.
%
An example input and output given by \leetcode is that given an input
$\cc{nums} = [ 0, 1, 0, 3, 12 ]$, a correct solution will return with
\cc{nums} storing the value $[ 1, 3, 12, 0, 0 ]$.

% Walk through the optimized solution.
\autoref{fig:movezeroes-code} contains two solutions to the
\movezeroes problem posted by independent coders.
% First solution: naive:
The first solution, \movezeroes0, implements a straightforward
approach.
%
After checking if its input is null or empty (line 5), \movezeroes0
iterates over \cc{nums} and stores each non-zero element at its final
index.
%
I.e., \movezeroes0 checks if the iterated value \cc{num} is non-zero
and if so, stores \cc{num} in \cc{nums} at a counter \cc{insertPos}
that is incremented (line 8).
%
After iterating once over \cc{nums}, \movezeroes0 stores $0$ in the
correct indices at the end of the array.
%
I.e., \movezeroes0 stores $0$ at all indices from \cc{insertPos} to
the end of the array.

% Talk about the optimized one.
The second solution, \movezeroes1, implements the following, less
obvious approach: declare a variable \cc{z} that stores the position
of the largest index of a non-zero value (line 4).
%
Iterate over the elements of \cc{nums} in increasing order.
%
In each iteration at index \cc{i}, if \cc{nums} at \cc{i} is
not zero (line 7), then increment \cc{z} (line 8) and swap in the
values stored in \cc{nums} at \cc{i} and \cc{z} (lines 6--9).

\subsection{Limitations of Current Approaches}
\label{sec:ex-current}
% Regression verification:
Although \movezeroes0 and \movezeroes1 are equivalent, and quite
succinct, it is surprisingly difficult for an automatic equivalence
checker to prover that the two programs are indeed equivalent.
%
Equivalence checkers that implement regression
verification~\cite{backes13,boehme13,felsing14,godlin09} typically rely
on matching similar control structures between a given pair of
programs.
%
However, the looping structure of the \movezeroes0 and \movezeroes1 is
fundamentally different: each execution of \movezeroes0 iterates over
its loop twice for a number of steps that is bounded only be the size
of input array, while each execution of \movezeroes1 iterates its
input array exactly once.

% Symbolic execution:
Approaches that symbolically execute the programs to quickly generate
tests that disprove equivalence~\cite{ramos11} can prove the absence
of executions that prove non-equivalence, up to a bound on the number
of control paths inspected.
%
However, both programs have an infinite set of feasible control
paths.
%
The symbolic representation of the effect of a bounded set of program
paths can be used to construct a trivial over-approximation of each
program~\cite{person08}, but without refining the abstraction of each
function directly using information required to prove equivalence, it
seems infeasible for over-approximations of each program computed in
isolation to be sufficient to prove their equivalence.

\subsection{Finding an independent proof of equivalence}
\label{sec:ex-approach}
% Define independent proofs of equivalence.
In our preliminary work, we have designed a new automatic analysis,
named \sys, that can often prove the equivalence of programs with
dissimilar control structure, independent of any bounds.
%
The key insight behind our approach is that for many programs, even
some that implement clever optimizations such as \movezeroes1 the
entire effect of the program can be described accurately using a
formula in a suitably expressive theory that is still amenable to
automatic reasoning.
%
In the case of the \movezeroes problem, the effect of \movezeroes0,
\movezeroes1, and in fact any correct solution, can be described by
formula relating initial final states that can be expressed in the
combination of the theories of linear arithmetic, uninterpreted
functions with equality, and arrays (referred to as \auflia).
%
Thus, in principle, a proof that \movezeroes0 is equivalent to
\movezeroes1 can be inferred that is structured as a proof that (1)
\movezeroes0 satisfies an invariant $R$ over initial and final states
(i.e., a \emph{transition invariants}) of correct solutions, (2)
\movezeroes1 satisfies a transition relation $R'$ (here equivalent to
$R$), and (3) any two programs that satisfy $R$ and $R'$ are
equivalent;
%
we refer to such a proofs as an \emph{independent} proof of the
equivalence of \movezeroes0 and \movezeroes1.

% Inferring independent proofs of equivalence.
Unfortunately, the observation that independent proofs of equivalence
exist does not give us sufficient information for how to infer them
automatically.
%
In particular, an equivalence prover that infers an independent proof
of the equivalence of \movezeroes0 and \movezeroes1, the prover must
infer sufficient transition invariants $R_0$ and $R_1$ that satisfy
each of the three conditions established above.
%
The second key insight behind the design of \sys is that sufficient
transition invariants $R_0$ and $R_1$ can be inferred by aggregating
the invariants of \emph{individual} control paths that prove that no
two pairs of witness the non-equivalence of $P_0$ and $P_1$.

% Refinement figure:
\begin{figure*}
  \includegraphics[width=1\linewidth]{fig/move-zeroes-refinement.pdf}
  %
  \caption{A control path $p_0$ of \cc{MoveZeroes0} (bottom) along with a
    control path $p_1$ of \cc{MoveZeroes1}.
    %
    Each occurrence of a control location in each path is annotated
    with a path invariant: path invariants of $p_0$ are typeset in
    blue, and path invariants of $p_1$ are typeset in red.
    % 
  }
  \label{fig:refinement-tree}
\end{figure*}

% Collect transition invariants from annotated path trees.
In other words, one effective strategy to search for suitable
transition invariants for \movezeroes0 and \movezeroes1 is to
enumerate a selection of the control paths each program in an
independent tree.
%
When our strategy enumerates a complete path in the trees of each
program, with $p_0$ a path of \movezeroes0 and $p_1$ a path of
\movezeroes1, the analysis checks if there is any run of $p_0$ and run
of $p_1$ from the same input that result in distinct outputs.
%
If so, then the analysis returns the runs as a counterexample to the
equivalence of the programs.
%
If not, the analysis extracts path invariants for $p_0$ and $p_1$ that
prove that each run of $p_0$ paired with each run of $p_1$ that starts
from the same initial state ends in the same final state, but which
are \emph{independent} in that each path invariant is defined purely
over the state of the program to which it belongs.

% Walk through the example refinement figure.
For example, \autoref{fig:refinement-tree} depicts the control path
$p_0$ of \movezeroes0 of all runs that execute the body of first loop
in \cc{MoveZeroes0} once and do not execute the body of the second
loop, along with the control path $p_1$ of \cc{MoveZeroes1} of all
runs that execute the body of the loop and \cc{if} statement exactly
once.
%
The path invariants of $p_0$ and $p_1$ are independent of each other,
but together, are sufficient to prove that all runs of $p_0$ and $p_1$
from equal states end in equal states.

% Going from path invariants to program invariants.
By extending the analysis with additional bookkeeping information that
maintains which program paths must still be enumerated, we can design
an effective equivalence prover the infers independent proofs of
equivalence, using the disjunction of path invariants along all
program paths as a suitable transition invariant.
%
The remaining technical issue is how to extract a proof, for any given
pair of control paths from two programs, independent path invariants
for each path that prove equivalence.
%
One contribution of our work is that this extraction can be reduced to
synthesizing a solution to a suitable \emph{tree-interpolation
  problem}~\cite{mcmillan06,heizmann10,albarghouthi12}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "p"
%%% End: 
