\section{Technical Background}
\label{sec:background}
% Outline section.
In this section, we review technical results on which our work is
based.
%
In \autoref{sec:foundations}, we review definitions and previous
results on the formal foundations of our work, including basic
definitions concerning functions, languages of sequences, logic, and
Craig interpolation.
%
In \autoref{sec:prog-lang}, we define a simple imperative programming
language, including the language syntax and semantics.
%
In \autoref{sec:flow-lang}, we define a simple language of flow
policies that can express declassification, and define the
policy-checking problem that we address in this work.

% Review definitions from formal logic.
\subsection{Formal Foundations}
\label{sec:foundations}

% Review foundational definitions, notation.
\subsubsection{Domains, Functions, and Sequences}
\label{sec:domains}
% Unordered pairs:
Our technical approach uses several standard definitions for reasoning
about functions and languages of sequences.

% Functions:
For a domain $D$, range $R$, function $f: D \to R$, domain element $d
\in R$, and range element $r \in R$, we denote the function that maps
$d$ to $r$ and maps every other domain element $d' \in D$ to $f(d')$
as $\upd{f}{d}{r}: D \to R$.
%
For each sequence of domain elements $D' \in D^{*}$ and sequence of
range elements $R' \in R^{*}$ of equal length, we denoted the
\emph{repeated update} of each domain element to its corresponding
range element as $\upds{f}{D'}{R'} = f [ d_0 \mapsto r_0] \ldots [
d_{n - 1} \mapsto r_{n - 1} ]$.

% Strings:
For a domain of symbols $D$, the space of all finite strings of $D$ is
denoted as $D^{*}$.
%
The empty sequence of symbols is denoted $\emptyseq$;
%
for strings $s, t \in D^{*}$, the concatenation of $s$ and $t$ is
denoted $s \concat t$.
%
The fact that $s$ is a prefix of $t$ (i.e., that there exists some
string $u \in D^{*}$ such that $t = s \concat u$) is denoted $s
\prefix t$.

% Define formal logic.
\subsubsection{Formal Logic}
\label{sec:logic}
Our approach models transformations over sets of program states using
formal logic.
% Define spaces of formulas.
The theory of linear arithmetic be denoted \lia, and for logical
variables $X$, the space of \lia formulas defined over $X$ is denoted
$\liaformulas[X]$.
%
For each formula $\varphi \in \liaformulas[X]$, let $\vocab(\varphi)$
denote the set of non-logical symbols that appear in $\varphi$ (i.e.,
the \emph{vocabulary} of $\varphi$).

% Define satisfaction and entailment.
For each interpretation $\iota$ of variables $X$ and formula $\varphi
\in \liaformulas[X]$ we denote that $\iota$ \emph{satisifes} $\varphi$
as $\iota \sats \varphi$.
%
For each set of formulas $\Phi \subseteq \liaformulas[X]$ and formula
$\varphi \in \liaformulas[X]$, we denote that $\Phi$ entails $\varphi$
under the axioms of $\lia$ as $\Phi \entails \varphi$.
%
For logical variables $x, y \in X$ and formula $\varphi$, $\varphi$
with each occurrence of $x$ replaced with $y$ is denoted
$\replace{\varphi}{y}{x}$.

% Stuff over pairs of vectors of logical variables of the same
% length.
For vectors of logical variables $X$ and $Y$, the concatenation of $X$
and $Y$ is denoted $X, Y$.
%
If $X$ and $Y$ are both of length $n$, then the \lia formula
constraining the equality of each element in $X$ with its
corresponding element in $Y$, i.e., $\bigland_{0 \leq i < n} \cc{x}_i
= \cc{y}_i$, is denoted $X = Y$.
%
The repeated replacement of variables $\varphi[ \ldots [ y_0 / x_0 ]
\ldots y_{n - 1} / x_{n - 1} ]$ is denoted $\replace{\varphi}{Y}{X}$.

% Hash out interpolation:
\subsubsection{Tree interpolation}
\label{sec:itps}
%
A tree-interpolation problem is a conjunction of formulas, structured
as a tree.
%
A solution to a tree-interpolation problem is a collection of formulas
that prove that the conjunction of all of the formulas is mutually
unsatisfiable, under \emph{locality} constraints on what symbols may
appear in the vocabulary of the collection.
%
{
\newcommand{\itps}{\mathcal{I}}
%
\begin{defn}
  \label{defn:tree-itps}
  % Define tree-interpolation problems.
  A \emph{tree-interpolation problem} is a triple $(N, E, C)$ in which
  % Nodes
  \textbf{(1)} $N$ is a set of \emph{nodes}.
  % Edges
  \textbf{(2)} $E \subseteq N \times N$ is a set of \emph{edges} such
  that the graph $T = (N, E)$ is a tree with root $r \in N$.
  % Constraints:
  \textbf{(3)} $C: N \to \liaformulas[X]$ assigns each node to a \lia
  \emph{constraint}.

  % Define interpolants for tree-interpolation problems.
  An \emph{interpolant} of $(N, E, C)$ is an assignment $\itps: N \to
  \liaformulas[X]$ such that:
  % root interpolant entails false
  \textbf{(1)} the interpolant at the root of $T$ is $\false$.
  % 
  I.e., $\itps(r) = \false$.
  % children interpolants entail node interpolant
  \textbf{(2)} For each node $n \in N$, the interpolants at the
  children of $n$ and the constraint at $n$ entail the interpolant at
  $n$.
  % 
  I.e., $\{ \itps(m) \}_{(m, n) \in E}, C(n) \entails \itps(n)$.
  \textbf{(3)}
  % vocabulary intersection
  For each node $n$, the vocabulary of the interpolant at $n$ is the
  common vocabulary of all descendants for $n$ and all non-descendants
  of $n$.
  % 
  I.e.,
  \[
  \vocab(\itps(n)) \subseteq
  % 
  \bigunion_{(m, n) \in E^{*}} \vocab(C(m))
  % 
  \intersection \bigunion_{(m, n) \notin E^{*}} \vocab(C(m))
  \]
\end{defn}
}

% Talk about solvers for the DAG interpolation problem.
Previous work has presented an algorithm \solvetreeitp that solves a
given tree-interpolation problem $(N, E, C)$ by invoking an
interpolating theorem prover on pairs of formulas with size
proportional to $|C|$ a number of times equal to
$|N|$~\cite{heizmann10}.
%
In general, such theorem provers may use time exponential in the size
of their input formulas, and construct interpolants of size
exponential in the size of their input formulas.
%
In practice, several useful heuristics have been developed that enable
interpolating theorem provers to efficiently find interpolants of size
close to the size of their input formulas~\cite{demoura08,mcmillan04}.

% Define the target language.
\subsection{A Simple Imperative Programming Language}
\label{sec:prog-lang}

% Define the language syntax.
\subsubsection{Syntax}
\label{sec:pl-syntax}
% Define the syntax of a program.
\begin{figure}
  \centering
  \begin{align*}
    % An instruction is:
    %
    % -an assignment of the value of an arithmetic expression,
    \instrs \assign\ & \cc{x := $\arithexprs$}\ 
    %
    |\ \assume(\boolexprs) \\
    % A Boolean expression is:
    %
    % -leq over arithmetic expressions,
    \boolexprs \assign\ & \arithexprs \leq \arithexprs\
    % -not of a Boolean expression
    |\ \lnot \boolexprs \\
    % -and of two Boolean expressions,
    |\ & \boolexprs \land \boolexprs\
    % -or of two Boolean expressions
    |\ \boolexprs \lor \boolexprs
  \end{align*}
  \caption{A grammar of \lang instructions.}
  \label{fig:syntax}
\end{figure}

% Define instructions.
A \lang program is a body of instructions the operate on integer
variables.
%
Let $\vars$ be a space of integer variables, and let the space of
arithmetic expressions over $\vars$ be denoted $\arithexprs$.
%
An \emph{instruction} either assigns the values of an arithmetic
expression to a target variable in $\vars$ or checks that a Boolean
expression holds.
%
A Boolean expression over integer variables $V$ is a Boolean
combination of less-then-or-equal predicates over arithmetic
expressions;
%
the space of Boolean expressions over $\vars$ is denoted
$\boolexprs$.

% Define programs.
Let $\locs$ be a space of control locations.
%
A \emph{control edge} is a pre-location, instruction, and
post-location;
%
i.e., the space of control edges is $\edges = \locs \times \instrs
\times \locs$.
%
A \emph{program} is an initial control location, a final control
location, and a set of control edges;
%
i.e., the space of programs is $\lang = \locs \times \locs \times
\pset(\edges)$ in which for each pair of control location $\cc{L},
\cc{L'} \in \locs$, there is at most one instruction $\cc{i} \in
\instrs$ such that $(\cc{L}, \cc{i}, \cc{L'})$ is in the set of edges.
%
For program $P \in \lang$, we will denote the initial location, final
location, and edges of $P$ as $\cc{i}^P$, $\cc{f}^P$, and $E^P$,
respectively.

% Define the semantics.
\subsubsection{Semantics}
\label{sec:pl-semantics}
%
A \lang program updates a state, which is a control location paired
with an evaluation of the variables in $\vars$.
%
In particular, a program \emph{store} is a map from each variable to
an integer;
%
i.e., the space of stores is $\stores = \vars \to \ints$.
%
A program \emph{state} is a control location paired with a store.
% 
I.e., the space of states is $\states = \locs \times \stores$.
% Define notation: evaluation.
For each arithmetic expression $\cc{A} \in \arithexprs$ and Boolean
expression $\cc{B} \in \boolexprs$, the value of $\cc{A}$ in $\sigma$
and the value of $\cc{B}$ in $\sigma$, denoted $\sigma(\cc{A}) \in
\ints$ and $\sigma(\cc{B}) \in \ints$ respectively, are defined in the
standard way.

% Define transfer functions of each instruction over states.
\begin{figure}
  \centering
  \begin{gather*}
    % Updates of stores:
    %
    % Assignment:
    \inference[A]
    %
    { }
    % 
    { 
      (\sigma, \cc{x := A} ) \stepsvar
      \upd{\sigma}{\cc{x}}{\sigma(\cc{A})}
    } \\
    \\
    % Assumption:
    \inference[B]
    %
    { \sigma(\cc{B}) = \true
    }
    %
    {
      (\sigma, \cc{\assume(B)}) \stepsvar 
      %
      \sigma
    } 
    % Program step:
    \inference[S]
    %
    { (\cc{L}, \cc{i}, \cc{L'}) \in E^P
      % 
      & (\sigma, \cc{i}) \stepsvar \sigma' 
      %
    }
    %
    { (\cc{L}, \sigma) \stepsto_P (\cc{L'}, \sigma')
    }
  \end{gather*}
  \caption{Inference rules that define, for each program $P \in
    \lang$, the runs of $P$.
    %
    Rules A and B define how each \lang instruction updates a program
    store.
    %
    Rule~S defines how each instruction updates state in 
    $P$.
    % 
  }
  \label{fig:semantics}
\end{figure}

% Define the runs of a program.
A run of a program is a sequence of states in which each pair of
consecutive states is in the transition relation of the program.
% Define update relation over stores.
The transition relation of a given program $P$ is defined by the
inference rules depicted in \autoref{fig:semantics}.
%
Instruction $\cc{i} \in \instrs$ \emph{takes} $\sigma \in \stores$ to
$\sigma \in \stores$, denoted $(\sigma, \cc{i}) \stepsvar \sigma'$ if
one of the following conditions holds.
%
An assignment \cc{x := A} takes store $\sigma$ to $\sigma$ updated to
bind \cc{x} to the value of \cc{A} in $\sigma$ (Rule~A).
%
An assume instruction \cc{assume(B)} takes store $\sigma$ to itself if
\cc{B} evaluates to $\true$ in $\sigma$ (Rule~B).

% Define step relation and runs.
State $(\sigma, \cc{L})$ \emph{steps} to state $(\sigma', \cc{L'})$ in
$P$ if $P$ contains a control edge $(\cc{L}, \cc{i}, \cc{L'})$ and
$\cc{i}$ takes $\sigma$ to $\sigma'$.
%
A \emph{run} of program $P = (i, f, E)$ is a sequence of states $r =
q_0, q_1, \ldots, q_n$ such that
%
(1) the control location of the initial state of $r$ is the initial
control location of $P$ (i.e., for some store $\sigma_0 \in \stores$,
$q_0 = (i, \sigma_0)$),
%
(2) the control location of the final state of $r$ is the final
control location of $P$ (i.e., for some store $\sigma_n \in \stores$,
$q_n = (f, \sigma_n)$), and
%
(3) each state in $r$ steps in $P$ to its successive state (i.e., for
each $0 \leq i < n$, $q_i \stepsto_P q_{i + 1}$).
%
The set of runs of $P$ is denoted $\runs(P) \subseteq \states^{*}$.

\subsubsection{Transition relations of instructions}
\label{sec:trans-rels}
% Define basic notions related to binary formulas, transition
% relations.
Each instruction in \lang can be modeled using a \lia formula.
%
Let $\vars'$ denote a set of primed copies of the program variables
$\vars$.
%
For all program stores $\sigma, \sigma' \in \stores$, let the $\lia$
model over logical variables $\vars$ that interprets each variable
$\cc{x} \in \vars$ as $\sigma(\cc{x})$ and interprets each variable
$\cc{x'} \in \vars$ as $\sigma'(\cc{x'})$ be denoted
$m_{\sigma,\sigma'}$.
%
Let a \emph{transition formula} be a formula over program variables
and their primed copies;
%
i.e., $\transformulas = \liaformulas[\vars, \vars']$.
%
For each instruction $\cc{i} \in \instrs$, there is a \emph{transition
  relation} $\transrel(\cc{i}) \in \transformulas$ such that for all
stores $\sigma, \sigma' \in \stores$ such that $\cc{i}$ takes $\sigma$
to $\sigma'$, $m_{\sigma,\sigma'}$ is a model of $\transrel(\cc{i})$.
% Define path invariants.
For each control path $p \in \paths$ and transition formula $\varphi
\in \transformulas$, if for all stores $\sigma, \sigma' \in \stores$
such that $p$ takes $\sigma$ to $\sigma'$, $m_{\sigma, \sigma'}$
satisfies $\varphi$, then $\varphi$ is a \emph{transition
  invariant}~\cite{podelski04} of $p$.

% Define path invariants.
For each control path $p \in \paths$ and transition formula $\varphi
\in \transformulas$, \emph{path invariants} $\invs: \paths \to
\transformulas$ for $p$ map each prefix of $p$ to a valid transition
invariant, in which the invariant for each path is supported by the
transition invariant of its prefix.
% Conditions for path invariants:
In particular: \textbf{(1)}
% Condition on initial location.
The invariant of the empty path is each pre-state variable is equal
to its corresponding post-state variable (i.e., $\invs(\emptyseq) =
(\vars = \vars')$).
% Inductive condition.
\textbf{(2)} For each prefix $p' \concat \cc{L} \concat \cc{L'}
\prefix p$, the path invariant of $p' \concat \cc{L}$ and the
transition relation of the instruction connecting $\cc{L}$ to
$\cc{L'}$ entails the path invariant of $p' \concat \cc{L} \concat
\cc{L'}$.
% 
I.e., for a copy $\vars''$ of $\vars$ disjoint from $\vars$ and
$\vars'$, $\invs(p \concat \cc{L})[ \vars'' / \vars' ],
\transrel(\cc{i})[ \vars'' / \vars' ] \entails \invs(p \concat
\cc{L} \concat \cc{L'})$.

% Defend against assumption that we don't handle actual PL features.
\subsubsection{Extending \lang to a practical language}
\label{sec:pl-practical}
% Some language features can be modeled in lang.
We will use \lang as a simple language of imperative programs that
still illustrates the key properties of our flow analysis.
%
Several features of a practical programming languages can be modeled
directly in \lang as presented.
%
Intra-procedural control flow structures in a practical language, in
particular \cc{if} statements, \cc{switch} statements, and \cc{while}
and \cc{for} loops, can be modeled in \lang with control edges
annotated with \cc{assume} instructions.
% 
Boolean values and variables can be modeled as integers that only take
on the values $0$ or $1$.

% Other features can't be modeled in lang.
Other features of practical programming languages cannot be modeled
easily in \lang as it has been presented.
%
Such features include
%
(1) richer control structures such as multiple (potentially recursive)
procedures and indirect control flow;
%
(2) richer state structure, including stores that map variables to
object and array values;
%
(3) richer instruction semantics, including non-linear operations and
system calls that read inputs and write outputs throughout the
execution of a program.

% Punt.
We will not discuss in detail how our analysis handles programs that
contain all of the above features, in order to simplify the
presentation.
%
However, the actual implementation of our analysis analyzes programs
represented in JVM bytecode, and models all of the above program
features accurately (see \autoref{sec:practical-languages}).

% Define a language of flow policies.
\subsection{A Language of Flow Policies}
\label{sec:flow-lang}
% Overview:
A information-flow policy $F$ defines an equivalence relation over the
input states of the program;
%
a program $P$ satisfies $F$ if an attacker who can observe the final
state of output can only determine the equivalence of the initial
state.
%
The formulation of information-flow policies as equivalence relations
has been established in previous
work~\cite{askarov07,backes09,benton04,terauchi05}.

% Define policy syntax.
Let $\vars_0$ and $\vars_1$ be disjoint copies of the program
variables $\vars$.
% Policy-language syntax.
A \emph{policy} is a \lia formula $\varphi \in \liaformulas[\vars_0,
\vars_1]$ that defines an equivalence relation over program states.
%
I.e., the language of policies is denoted $\flowpols =
\liaformulas[\vars_0, \vars_1]$.

% Define program-policy satisfaction.
For each $\lang$ program $P = (\cc{L}_0, \cc{L}_f, E)$ and each flow
policy $F \in \flowpols$, $P$ \emph{satisfies} $F$ (denoted $P \sats
F$) if for all pairs of runs of $P$, $[ (\cc{L}_0, \sigma_0^0),
\ldots, (\cc{L}_f, q_m^0) ]; [ (\cc{L}_0, \sigma_0^1), \ldots,
(\cc{L}_f, \sigma_n^1) ] \in \runs(P)$, if the initial states of the
runs satisfy $\varphi$ (i.e., if $\sigma_0^0, \sigma_0^1 \sats
\varphi$), then the final states of the runs are equal (i.e., then
$q_m^0 = q_n^1$).
%

% Put this definition in the context of known definitions, so people
% don't think we're crazy.
The above definition is a termination-insensitive variation of gradual
release~\cite{askarov07}.
% Reduce label-based models to ours, claim this is prior work.
A standard label-based non-interference
policy~\cite{denning76,myers99,myers97,myers98} over the two-point
lattice $L \sqsubset H$ can be expressed as a $\flowpols$ policy as
follows: for $\cc{X}^L$ the set of input variables annotated $L$,
generate the $\flowpols$ policy $X^L_0 = X^L_1$~\cite{askarov07}.
%
This construction can be generalized in a straightforward way for
arbitrary label lattices.

% Generalize to different models.
The definition of $\flowpols$ that we use to present this work models
a powerful attacker who can distinguish any pair of distinct output
states.
%
However, $\flowpols$ can be generalized to model weaker attackers who
can only distinguish output states by the values of particular
predicates over the states.
%
$\flowpols$ can be generalized to a policy language in which each
policy is a \emph{pair} of equivalence relations over states,
$\varphi$ and $\varphi'$.
%
A program satisfies $(\varphi, \varphi')$ if an attacker who can
only distinguish states that are not equivalent under $\varphi'$
cannot distinguish input states that are equivalent under $\varphi$.
%
The given definition of $\flowpols$ is the instance of the more
general definition in which $\varphi'$ is fixed to be the equality
relation over final states.

% Limitations:
However, even the general form of $\flowpols$ can only express desired
guarantees against an attacker who can only observe the values output
by a program.
%
$\flowpols$ cannot, however, express desired guarantees against an
attacker who can observe whether or not a program will terminate
(i.e., termination-sensitive non-interference~\cite{bohannon09}) or
observe computational resources used by a program, including execution
time~\cite{brumley03,coppens09,kocher96} or system
caches~\cite{doychev13}.

% Define the checking problem.
The problem that we address in this work is to decide if a given
program satisfies a given flow policy.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "p"
%%% End: 
