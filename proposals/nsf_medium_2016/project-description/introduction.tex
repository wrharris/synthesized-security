% introduction:
\section{Introduction}
\label{sec:introduction}

% fundamental problem:
Developing secure programs remains a critical and open problem:
hundreds of new security vulnerabilities are discovered in
system-critical software every year.
%
The problem is exacerbated by the fact that new models under which a
program may feasibly be attacked, and new primitives that a system can
provide that a program can potentially use to defend itself, are
constantly proposed, many motivated by secure issues raised by cloud
computation~\cite{almeida16,schuster15,sgx,sinha15,sinha16,trustzone}.
 
% existing work: ad-hoc synthesis
To address this problem, the security community has proposed various
semi-automated techniques (i.e., program \emph{synthesizers}) for
generating or instrumenting programs to be secure in particular
contexts.
%
In previous work, we have developed secure-program synthesizers for
several contexts.
%
In particular, we have developed synthesizers that synthesize programs
that securely instrument a given program to use hooks into Linux
Security Modules~\cite{ganapathy05,ganapathy06}, to use system-level
capabilities~\cite{harris13}, and to use system-level DIFC
primitives~\cite{harris10}.
%
We have also developed synthesizers that synthesize equivalent
programs and applied them to deobfuscate malware~\cite{jha10} and
developed obfuscators that provably obfuscate desired predicates about
system state~\cite{wu16}.

% problem: individual synthesizers have to reinvent a lot of work
While the individual synthesizers developed in previous work each have
demonstrated the potential for automating secure programming, this
approach to developing synthesizers has two significant limitations.
%
First, a synthesizer developer must define from scratch its own language
of security specifications (i.e., \emph{policies}).
%
Second, the developer must define to what extent an instrumented
program may differ from the behavior of a given program.

% problem context:
Outside of the context of generating secure programs, we have also
developed a framework that generates synthesizers themselves.
%
Prior to our work, significant work had produced a variety of program
synthesizers that each address a particular problem in program
synthesis.
%
Each synthesizer had been developed to each take specifications in a
variety of forms, including logical formulas and
examples~\cite{gulwani11,gulwani12,harris11,jha10,kuncak12,schkufza13,solar-lezama05,srivastava10,udupa13}.
%
Each synthesizer then generated a program using an algorithm developed
independently from scratch.
% key result:
The key result of our work was a framework for developing program
synthesizers, called \emph{Syntax-Guided Synthesis
  (SyGuS)}~\cite{alur13}.
%
We showed that each of the individual specification languages
developed in previous work could be defined as an instance of the
specification language supported by SyGuS.
%
In addition, we showed that each synthesis algorithm proposed in
previous work could be defined as the \emph{meta}-algorithm included
in SyGuS, instantiated on a particular specification of how a
program's syntactic constructs may be combined to complete incomplete
holes in a given program skeleton.

% limitations of sygus:
The development of secure-program synthesizers would be improved
significantly if they could be generated automatically as instances of
a synthesizer framework, such as SyGuS.
%
However, the problem of synthesizing a secure program differs
fundamentally from synthesis problems addressed by conventional
synthesis frameworks.
%
First, security specifications, unlike typical safety properties of
programs (such as a memory safety), are often difficult to formalize
because they must be defined over all \emph{sets} of program runs,
rather than over each run in isolation~\cite{clarkson10}.
%
Second, conventional synthesis problems can be formulated as
completing undefined \emph{syntactic} holes in a program, which
naturally generalizes problems such as completing the core logic of
the kernel of a scientific computation.
%
However, synthesizing a practical, secure program typically amounts to
taking as input a complete program and rewriting the program to
execute instructions that in particular cases may remove existing
program functionality.
%
I.e., secure synthesis is typically defined using a notion of weak
\emph{semantic} equivalence to a given original program.

% what we'll do:
We propose to develop a unified framework, named \sys, for generating
secure-program synthesizers.
%
\sys will address the limitations of current frameworks for program
synthesis by \textbf{(1)} developing a meta-specification language for
expressing properties of sets of program runs and \textbf{(2)}
developing synthesis algorithms that ensure that a given program
satisfies rich \emph{relational properties}.
%
Such relational properties will be used to express both desired
properties over multiple runs of a synthesized program and properties
relating the runs of a synthesized program to runs of a given
original program.

% developing instances
We will apply \sys to obtain synthesizers that address several
emerging problems in secure programming.
%
In particular, we will obtain synthesizes that generate programs that
securely use hardware primitives for creating and using Secure
Isolated Regions (SIRs), that are free of critical side channels, and
system service libraries that are free of Denial-of-Service
vulnerabilities.
%
The new synthesizers will be built by instantiating \sys using
techniques developed in our previous work on verifying the security of
programs that use SIRs~\cite{sinha15,sinha16} and verifying that a
given program satisfies a given complexity (i.e., timing)
bound~\cite{srikanth17}.

% proposal outline:
The rest of this proposal is organized as follows.
%
\autoref{sec:previous-work} reviews previous work that we have
performed on synthesis for security and synthesis frameworks.
%
\autoref{sec:proposed-work} describes work on synthesis for security
that we propose to perform in the future.
%
\autoref{sec:broader-impacts} describes the broader impacts of our
proposed work.
%
\autoref{sec:prior-results} describes the results of prior work
performed by the PI's funded by the NSF.
%
\autoref{sec:work-plan} provides a plan for the proposed work.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "p"
%%% End: 
