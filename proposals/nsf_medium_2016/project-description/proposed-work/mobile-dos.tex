\subsubsection{Synthesizing Denial-free System Services}
\label{sec:mobile-dos}
% context:
Mobile devices have emerged as one of the most important computing
platforms of the next generation.
%
The \emph{Android} operating system is the single most popular
operating system for mobile devices, currently deployed in over one
billion devices worldwide.
%
A key feature of the Android development framework is that it provides
system services that support a high-level interface that abstracts
many complicated details of how they are implemented at the system
level.
%
For example, the \cc{ActivityManager} service maintains all
system-level state for each app component that executes on a system;
%
the \cc{PackageManager} service implements operations that abstract
all resources used to install or remove a package from a system.

% problem, conceptually:
Unfortunately, because Android provides a single, shared codebase of
coarse-grain operations with complex implementations, it can
potentially contain critical Denial of Service vulnerabilities that
can be triggered by even an unprivileged attacker.
%
In particular, if an implementation of a high-level service acquires
and holds a lock while using an unacceptably long time to execute,
then it will effectively break the functionality of all other apps
executing on the system that request another service whose
implementation requires the same lock.

% problem, say more:
Previous work has demonstrated that this is not only a problem in
principle, but a serious problem that is prevalent in practical
implementations~\cite{huang15}.
%
In particular, previous work uncovered four distinct DoS
vulnerabilities in the current implementation of Android services by
introducing code analyzers that answered basic queries about the
Android codebase to guide manual analysis.
%
The key design component of Android services exploited in each of the
attacks is that of the service's \emph{Watchdog thread}.
%
The Watchdog thread periodically checks the status of all locks
acquired by services.
%
If the Watchdog finds that a particular process has held a particular
lock for an amount of time that exceeds a threshold, then the
Watchdog restarts the entire service process.

%
While Watchdog threads were originally implemented to improve system
robustness, the attacks discovered in previous work use them as one
component of a novel class of vulnerabilities, named \emph{Android
  Stroke Vulnerabilities (ASV)}.
%
An ASV consists of a service that acquires a lock, and before
releasing the lock, performs some computation that, depending on
external inputs, may use an amount of time greater than the amount of
time allowed by a Watchdog.
%
An attacker crafts such an input and then repeatedly provides it to
the Android service module, causing the service module to repeatedly
take an unexpectedly-long amount of time to service the request and
then cause the Watchdog to restart the service process.

% characterize complexity, current techniques, their limitations
Even identifying such vulnerabilities in practical systems requires
intricate manual analysis because the vulnerabilities can only
triggered by exploiting code that lies deep within service
implementations (typically only accessible within approximately 10
function calls)~\cite{huang15}.
% proposed work: interface:
We propose to extend existing work on identifying DoS attacks in
mobile services by applying \sys to generate a synthesizer that can
automatically synthesize implementations of services that are free of
DoS vulnerabilities.
%
The produced synthesizer will take as input \textbf{(1)} a reference
implementation $P$ of services and \textbf{(2)} a threshold $t$ under
which each critical section must execute and synthesize an
implementation $P'$ of services that is functionally equivalent to
$P$, but in which each critical section executes in time under than
$t$.

% proposed work: implementation:
To synthesize $P'$ from $P$, the synthesizer will split individual
critical sections in $P$ into multiple critical sections of $P'$.
%
Splitting critical sections so that the result is a robust
implementation is a non-trivial programming problem.
%
In particular, the synthesizer must ensure that when it splits a
critical section, it generates sufficient operations to backup and
restore shared state so that following critical sections execute
\emph{correctly}, even if another thread is interleaved to execute
particular critical sections.
%
However, for the synthesized program to perform efficiently, the
synthesizer must \emph{minimize} the resulting number of split
critical sections.

% limitations of existing solvers:
Existing synthesizers, such as those in the SyGuS
framework~\cite{alur13} cannot be directly applied to generate such a
synthesizer due to multiple fundamental limitations.
%
First, such synthesizers do not currently contain techniques for
reasoning robustly about equivalence.
%
Second, such synthesizers require a synthesis problem to be formulated
as synthesizing constructs for a set of specified syntactic holes;
%
the problem of splitting critical section cannot be succinctly
formulated using such a set.
%
Third, such synthesizers do not currently contain techniques for
reasoning about correctness guarantees specified in terms of program
performance.

% say what our synthesizer will do
The synthesizer that we will develop over the course of our proposed
work will address the above technical challenges.
%
The first and second challenges will be addressed by \sys's algorithms
for synthesizing relational invariants.
%
The third challenge will be addressed by extending techniques that we
have recently developed to verify that a given program satisfies a
given complexity bound~\cite{srikanth17}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../p"
%%% End: 
