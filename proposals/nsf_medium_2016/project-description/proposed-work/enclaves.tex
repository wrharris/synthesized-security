\subsubsection{Synthesizing correct usage of Secure Isolated Regions}
\label{sec:sirs}
% context:
Modern computer users can perform resource-intensive computations on
their sensitive data on networked cloud servers that provide
computation as a service.
%
In particular, a cloud server may run vulnerabile systems
infrastructure that can be compromised by a user who runs an
application co-located with a users computation.
%
A malicious user may then exploit the vulnerability to learn
information about another user's private data, or tamper with the
result of a user's critical computation.

% SIR/enclave primitives:
Such architectures provide special instructions that an application
invokes to contain segments of code and data in hardware-level
\emph{Secure Isolated Regions} (SIRs).
%
In particular, the SGX feature supported by many modern Intel
processors~\cite{sgx} and the TrustZone feature supported by many
modern ARM processors~\cite{trustzone} support an extended
instruction-set architecture (ISA) that a user-level application can
use to ensure confidentiality and integrity.
%
Code that executes in a SIR cannot be tampered with by other agents
on a system.
%
Data in a SIR is encrypted when written to memory, ensuring that even
an adversary with complete control over hardware connected to the core
processor cannot learn information about the application's data or
tamper with the data without being detected.

% include example code that uses SIRs
\begin{figure}[t]
  \centering
  \begin{floatrow}
    % include gzip code:
    \ffigbox[0.48\textwidth]{ \input{code/reduce.cpp} }
    {\caption{\cc{reduce}: performs a reduction, in the context of a
        map-reduce framework. }
      \label{fig:reduce} }

    % include policy:
    \ffigbox[0.48\textwidth]{ \input{code/reduce-sec.cpp} }
    {\caption{\cc{reduce_sec}: a verified version of \cc{reduce},
        adapted to use secure channels.} 
      \label{fig:reduce-sec} }
  \end{floatrow}
\end{figure}

% specific problem addressed in previous work
Such hardware features are powerful~\cite{schuster15}, but present
application programmers with new, significant challenges.
% walk through the example naive reducer:
\autoref{fig:reduce} contains pseudocode for a simple \emph{reduce}
procedure, named \cc{reduce}, that could potentially be run on a cloud
server as a component in a map-reduce framework.
%
The core functionality of \cc{reduce} is to read as input a sequence
of numeric values as input (lines 3---9), compute their sum (lines
11---15), and output the sum.
%
While the core functionality is relatively simple, the implementation
of \cc{reduce} is complicated by the fact in order for it to trust the
integrity of its inputs, it must establish a encrypted input channel
and read values from the input channel (lines 3---8).
%
Similarly, in order for \cc{reduce} to establish trustworthiness of
its output, it must establish an encrypted output channel and write
values to the output channel (lines 18---19).
%
Both channels are established using several low-level operations on
memory that, if incorrect, could compromise the correctness of
\cc{reduce}.
%
Moreover, \cc{reduce} must not inadvertently directly store a value
with information about its sensitive inputs to an address outside of
its SIR.

% review previous work
\paragraph{Previous work on verification}
% walk through Sanit's previous work:
We have performed previous work on verifying that an application uses
SIRs correctly~\cite{sinha15,sinha16}.
%
In particular, our work addresses the problem of taking \textbf{(1)}
an application manually written to use SIRs and \textbf{(2)} a policy
denoting the sensitive information operated on by the application and
verifying that the application does not leak sensitive information to
an adversarial system.
%
The key result of our work is a semi-automated design methodology that
a programmer follows to write an application that satisfies
confidentiality using SIR's.
% step (1): use a verified communication library
In particular, the programmer writes the program to use a
communication library---one artifact resulting from the work---to
marshal the communication of all sensitive information to its
environment.
%
We manually verified that the library reveals given information only
to target storage.

% step (2): 
The programmer also runs an automatic verifier---a second artifact
resulting from the work---on code assigned to run in a SIR to ensure
that it only releases sensitive information through the verified
communication library.
%
Verifying the property amounts to verifying that code designated to
execute in a SIR only \textbf{(1)} runs code designated to execute in
a SIR (as opposed to, e.g., incorrectly branching to untrusted code)
and \textbf{(2)} only writes sensitive information to memory addresses
within a SIR.
%
When successful, the verifier automatically generates a formal proof
of security.

% walk through the revised code:
\autoref{fig:reduce-sec} contains \cc{reduce} revised to use a secure
channel developed in our work.
%
The implementation of core functionality in \cc{reduce}
(\autoref{fig:reduce}, lines 9---15) is identical to \cc{reduce_sec}
(\autoref{fig:reduce-sec}, lines 6---12).
%
However, all code in \cc{reduce} that manually establishes secure
input (\autoref{fig:reduce}, lines 3---8) and output channels (lines
18---19) are replaced with usage of the verified \cc{Channel} library
(\autoref{fig:reduce-sec}, lines 3 and 15).

% propose new work
\paragraph{Proposed work on synthesis}
%
Our previous work enables a programmer to verify that a program that
has already been \emph{manually} written to use SIRs satisfies a
desired confidentiality policy.
%
However, it does not address the problem of writing or rewriting an
application to correctly use SIRs.
%
Writing an application to correctly use SIRs typically requires a
developer to write the application to execute low-level instructions
so that multiple goals are satisfied simultaneously.
%
In particular,
% enough to get the job done:
\textbf{(1)} the code segments designated to execute in a SIR must be
sufficiently \emph{expansive} that they can perform desired
computations on sensitive inputs.
% not too much to not be secure:
\textbf{(2)} The code segments designated to execute in a SIR must be
sufficiently \emph{compact} that they do not leak information about
the sensitive information that they operate on, and that such a
property can be verified automatically.
% share efficiently:
\textbf{(3)} Data that must be operated on by both code that executes
in a SIR and code that does not execute in a SIR must be \emph{shared
  efficiently}.
% integrity:
\textbf{(4)} Code that executes in a SIR but receives data not stored
in a SIR must run sufficiently strong \emph{validation} checks on such
data (which can be tampered with by the adversary in arbitrary ways)
to ensure that it can use the data while preserving the desired
\emph{integrity} of the computation that it performs.
% functionality:
\textbf{(5)} Such validation checks must be sufficiently
\emph{permissive} that if code that executes out of an SIR is not
corrupted by an adversary, then the entire application correctly
performs desired \emph{functionality}.

% revisit previous work on verification:
Our previous work on the verification of applications that use SIRs is
based on the assumption that a programmer has already written an
application to satisfy each of the above conditions.
%
Given such an application, our verifier validates that the program
uses SIRs in order to satisfy conditions \textbf{(1)} and
\textbf{(2)}.
%
However, it does not attempt to verify that the program shares data
efficiently (condition \textbf{(3)}) or that it satisfies desired
integrity guarantees (conditions \textbf{(4)}).
%
In fact, integrity properties of practical programs are typically
difficult even to \emph{express} because each is typically highly
specific to its individual application.
%
Finally, because the verifier operates only on code that has been
adapted to use SIRs, but not the original code as a reference, the
verifier cannot be used to verify that the use of SIRs preserves
critical functionality (condition \textbf{(5)}).
%
Specifications of core functionality that must be preserved, similar
to desired integrity properties, are typically each also highly
specific to individual applications.

% proposed work:
We will apply \sys to generate a synthesizer that takes an application
that uses no SIR primitives and instruments it to use SIR primitives
so that it satisfies each of conditions \textbf{(1)}---\textbf{(5)}.
%
We expect that developing such a synthesizer will be feasible based on
key observations that connect the problem with our previous work on
developing synthesis frameworks.
% synthesizers as loops on verifiers
In particular, a key result of our previous work developing the SyGuS
synthesis framework is that a program synthesizer can often be
structured as a procedure that iteratively synthesizes a candidate
program, runs a verifier to determine if the program satisfies key
properties, and uses the result of the verifier to generate a next
candidate program (\autoref{sec:program-syn}).
% 
We believe that it is highly feasible that we can develop a
synthesizer that synthesizes programs that satisfy properties
\textbf{(1)} and \textbf{(2)} by iteratively running the verifier for
SIR programs that we have developed in previous work and inspecting
its results to synthesize new candidate uses of SIR primitives.
%
Rich integrity properties (conditions \textbf{(4)}) will be expressed
as hyperproperties~\cite{clarkson10}.
%
Equivalence to an original program will be established by synthesizing
relational invariants that accompany the synthesized program.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
