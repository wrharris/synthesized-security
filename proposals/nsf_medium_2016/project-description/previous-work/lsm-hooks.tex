% hook placement:
\subsubsection{Synthesizing Correct Usage of Security Hooks}
\label{sec:syn-hooks}

% example code, drawn from VFS:
\begin{figure*}
  \begin{minipage}{0.48\linewidth}
    \input{vfs.c}
  \end{minipage}
  %
  \begin{minipage}{0.48\linewidth}
    \input{rmdir.c}
  \end{minipage}
  \caption{Virtual Filesystem (VFS) code from
    \cc{linux-2.4.21/fs/namei.c} that removes a directory without
    checking a Linux Security Module (LSM).}
  \label{fig:vfs}
\end{figure*}

% problem context: LSM and hooks
The Linux security modules (LSM) framework~\cite{wright02} is a
generic framework which allows diverse authorization policies to be
enforced by the Linux kernel.
%
Its goal is to ensure that securitysensitive operations are only
performed by users who are authorized to do so.
%
It consists of a reference monitor~\cite{anderson72}, which
encapsulates an authorization policy to be enforced, and hooks, which
define the interface that the reference monitor presents.
%
Calls to these hooks are placed at several locations within the Linux
kernel, so that they mediate security-sensitive operations performed
by the kernel.
%
Each hook call results in an authorization query to the
reference monitor.
%
The kernel performs the security-sensitive operation only if the
authorization query succeeds.

% discuss the VFS code in the example
\autoref{fig:vfs} contains virtual filesystem (VFS) code selected from
the Linux kernel that removes a given directory stored in variable
\cc{dir} from a parent stored in \cc{dentry} (for now, consider only
the lines \autoref{fig:vfs} that are not explicitly labeled with a
label of the form $\cc{H}i$;
%
these lines will be discussed below).
%
Such code should only remove a directory successfully when run by a
process that contains a particular combination of permissions.
%
In particular, the process should hold the rights to \textbf{(1)}
search \cc{dentry}, \textbf{(2)} write to \cc{dentry}, and, in
particular, \textbf{(3)} remove a directory from \cc{dentry}.

% discuss the hooks in the example:
LSM provides a module that enforces such a policy, provided that
particular entry functions (i.e., \emph{hooks}) to the module are
called to query the module as to whether a process has a given
permission.
%
In \autoref{fig:vfs}, the lines explicitly labeled with a label of the
form $\cc{H}i$ are calls to LSM hooks that enforce the desired
policy.

% problem:
Prior to our work on the topic, the decision as to where to place
hooks was often made informally, and hooks were placed manually at
locations deemed appropriate in the Linux kernel or user-level
application.
%
This process suffers from several drawbacks.
%
In particular, inadvertent bugs in placing hooks can induce security
holes~\cite{jaeger04,zhang02}.
%
Furthermore such code can be adapted to reflect modifications to code
or a desired policy only with careful reasoning.

% our contribution:
In previous work, we proposed an automatic technique, named \tahoe,
that automatically generates hook calls that, by construction, satisfy
the policy enforced by an LSM module~\cite{ganapathy05}.
%
Our technique requires two inputs: the Linux kernel, and the reference
monitor (i.e., the kernel module that implements it) which contains
the source code for authorization hooks.
%
It analyzes them and identifies locations in the kernel where hooks
must be placed so that security-sensitive operations are authorized
correctly.
%
In particular, given the version of \autoref{fig:vfs} that does not
contain calls to hooks and an implementation of the LSM that provides
implementations of the hooks, \tahoe inserts the hook calls in
\autoref{fig:vfs} automatically.

% walk through the technique
The key idea behind \tahoe is to leverage semantic information
embedded in the source code of the hooks and the Linux kernel.
%
\tahoe uses static analysis to determine the set of operations
authorized by each hook.
%
A similar analysis on the kernel-side determines the set of operations
performed by each kernel function.
%
The results of hook analysis and kernel analysis are then merged to
construct an authorization graph.
%
An authorization graph relates each kernel function to the set of
hooks that must protect it.
%
With the authorization graph in hand, hook placement is
straightforward: at each location in the kernel where a kernel
function is called, insert hooks (as determined by the authorization
graph) that must protect the function.
%
\tahoe thus addresses all the problems discussed above.
%
First, because \tahoe analyses the operations performed by a kernel
function to obtain the set of hooks that must guard each operation, it
ensures correctness by construction.
%
Second, because \tahoe is general-purpose and analyzes both hook and
kernel code, it extends easily to new security policies and emerging
applications alike.

% evaluation:
To evaluate the effectiveness of \tahoe, we used it to generate hook
calls in a version of a Linux kernel that did not previously contain
calls to hooks, and compared the results of \tahoe to calls to hooks
that were inserted manually.
%
In summary, we found that \tahoe could replicate, and in some cases
improve upon, the performance of manual coding, with a manageable set
of hook-placement mistakes.

% cite paper on user-level stuff
In further work~\cite{ganapathy06}, we developed an analogous
technique that can be applied to instrument user-level programs that
service requests from multiple users.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../p"
%%% End: 
