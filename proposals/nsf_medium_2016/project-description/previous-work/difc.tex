% policy weaving for DIFC:
\subsubsection{Synthesizing correct usage of DIFC labels}
\label{sec:syn-difc}

% Give background on the HiStar DIFC system.
\paragraph{Background on the HiStar DIFC system}
% Give an overview of HiStar.
HiStar, a Decentralized Information-Flow Control (DIFC) operating
system~\cite{zeldovich06}, provides primitives that an application can
invoke to protect the secrecy and integrity of its sensitive
information.
%
In particular, the HiStar kernel maps each process and object on the
system to a label.
%
Each time a process $p$ attempts to access an object $o$, HiStar
interposes the access, and only allows the access if the labels of $p$
and $o$ satisfy particular constraints.
%
A process can use the primitives provided by HiStar to update the
labels of system objects, subject to particular constraints.
%
We now discuss these constraints in detail.

% Talk about the structure of HiStar.
HiStar maintains a rooted graph of objects, i.e., processes and files,
where each object is bound to a \emph{label}.
%
A label is a map from each element in the space of \emph{categories}
maintained by HiStar to one of three levels: $\lowlv$, $\midlv$, and
$\highlv$, ordered as $\lowlv < \midlv < \highlv$.
%
A label $L_0$ \emph{flows to} label $L_1$ over categories $C$ if each
category $c \in C$ has a level in $L_0$ less than or equal to its
level in $L_1$.
%
The HiStar kernel maps each object $o$ to a label $L_o$, and maps each
process $p$ to a \emph{declassification} $D_p$.
%
$D_p$ holds a set of categories that HiStar ignores when determining
whether or not to allow an access attempted by $p$.
%
A process $p$ can read from (write to) a file $f$ if $L_p$ flows from
(to) the label of $L_f$ over all categories not in $D_p$.
%
$p$ can create a file with label $L_f$ linked from directory $d$ if
(1) $L_p$ flows to $L_f$ and (2) $p$ can write to $d$.
%
A process may at any time create a fresh category $c$, at which point
it is the only process that declassifies $c$.

% Introduce gates.
A process can create a \emph{gate}, which is a program module that
another process $p$ can execute to perform fixed operations on
sensitive information.
%
Code bound to a gate thus serves a role similar to the role served by
\emph{secure isolate regions} on particular architectures (see
\autoref{sec:sirs}).
%
A process $p$ can create a gate $g$ with label $L_g$ and
declassification $D_g$ if both (1) $L_p$ flows to $L_g$ over
categories not in $D_p$ and (2) $D_g$ is contained in $D_p$.
%
A process $q$ can then execute $g$ with declassification $D \subseteq
D_q \union D_g$.
%
Moreover, $q$ can execute gate $g$ with a label $L$ such that $L_q$
and $L_g$ flow to $L$ over all categories not in $D$.

% Punt on actual implementation.
The complete design of HiStar is more complex than the system
described above: it includes \emph{four} levels, additional
\emph{clearance and verify labels} for processes and gates, and
primitives that a process can invoke to update its label throughout
its execution (not just when calling a gate).
%
We omit descriptions of these features to simplify the presentation of
our approach, but the actual implementation of the approach described
in this dissertation supports such features.

% Describe the authlog program.
\paragraph{\authlog: an append-only logging service}

% Give code for the running example.
\begin{figure}
  \centering
  \begin{floatrow}
    \ffigbox[0.48\textwidth]{
      \input{log-init.c} }
    {\caption{\authlog: a collection of \difclang modules, \loginit and
        \logger, that implement an append-only log service.
        % 
        \loginit initializes a log object at link \logfile and a gate
        whose module is \logger.
        % 
        \logger reads a message from the object at link \msg, and appends
        the value read to \logfile.}
      % 
      \label{fig:authlog} }
    %
    \ffigbox[0.48\textwidth]{
      \input{logger.c}
      % 
      \input{log-clients.c} }
    {\caption{Pseudocode for \logclient, a cooperative client of
        \authlog, and \malclient, a malicious client of \authlog.} %
      \label{fig:log-clients} }
  \end{floatrow}
\end{figure}

% Point to the pseudocode, and establish conventions independent of
% any particular module.
\autoref{fig:authlog} contains pseudocode for a program \authlog, which
uses HiStar label operations to maintain a log file and provide a gate
that \authlog's environment can use to append to the log file.
%
For now, ignore the lines highlighted in gray: these are the
instrumentation code introduced by our technique, and are described
below.

% Describe modules of authlog.
\authlog consists of two modules: \loginit and \logger.
%
Program modules are called asynchronously by an environment, which by
convention provides to a module a \emph{return gate} linked from the
object symbol \cc{RET}.
%
By convention, when the module completes execution, it calls the
return gate to return control to its environment.
%
\loginit loads the root object into an object variable (line
\cc{L0}),
%
creates a log file linked from the root at symbol \cc{LOG} (line
\cc{L1}),
%
creates a gate linked from the root at symbol \cc{LOGGER} and
bound to the \logger module (lines \cc{L2}),
%
loads the return gate into object variable \cc{ret} (line
\cc{L3}),
%
and returns control to its environment by calling the return gate
(line \cc{L4}).
%
\logger appends the message value in \cc{msg} to the log file (lines
\cc{L5}--\cc{L8}), and returns control to its environment by calling
the return gate (lines \cc{L9}--\cc{L10}).

%
\autoref{fig:authlog} also contains pseudocode for two clients of
\authlog: \logclient and \malclient.
% 
\logclient cooperates with \logger to append to \logfile: \logclient
writes a new message to \msg (lines \cc{C0}--\cc{C2}) and
calls the \logger gate to append the message to \logfile (lines
\cc{C3}--\cc{C4}).
% 
\malclient attempts to violate the integrity of \logfile by
writing a message directly to \logfile (line \cc{M0}).

% Describe the policy for authlog.
\paragraph{Policies for \authlog}
%
Our goal is to automatically instrument \authlog to use label
operations to provide sufficient access rights when interacting with a
cooperating environment, but satisfy non-interference when interacting
with a malicious environment.
%
In particular:
\begin{enumerate}
\item
  \label{it:read-log} % environment should be able to read from log
  After \authlog's environment executes \loginit, the environment
  should be able to read from \logfile.
\item \label{it:func} % authlog doesn't screw up reading and writing.
  If \logger is entered in a state in which it can read from \msg and
  read from and write to \logfile, then it successfully reads from
  \msg and appends to \logfile.
  %
  If \logger then returns control to its environment, then the
  environment can read from \logfile.
\item \label{it:non-int}
  % authlog preserves the integrity of the logfile.
  Information does not flow from the environment to \logfile, except
  through the value in \msg read by \authlog.
\end{enumerate}

% Give the DIFC policy for authlog.
\begin{figure}
  \centering
  \begin{floatrow}
    \ffigbox[0.48\textwidth]{
      \includegraphics[width=1\linewidth]{logger-ar.pdf} }
    {\caption{\logar: a DIFC policy
        for \authlog.
        % 
        \logar accepts traces of \difclang states in which \authlog
        executes operations on objects or transfers control to its
        environment with insufficient access rights. } 
      \label{fig:authlog-ac} }
    %
    \ffigbox[0.48\textwidth]{
      \includegraphics[width=1\linewidth]{authlog-ni.pdf}}
    {\caption{\logni: a taint policy for \authlog.} 
      \label{fig:authlog-ni} }
  \end{floatrow}
\end{figure}
%
The requirements for \authlog can be expressed as a pair of policy
automata.
%
The policy specifying the \emph{access rights} to read from and write
to files that the program must hold when \authlog transfers control to
its environment is represented as a finite-state automaton over an
alphabet of conditions on \difclang states.
%
A trace of \difclang states $t$ violates a DIFC policy $F$ if the
states of $t$ satisfy a trace of conditions that is accepted by $F$.

% Walk through the DIFC policy for authlog.
\autoref{fig:authlog-ac} contains a DIFC policy \logar that explicitly
expresses the desired access-rights policy for \authlog.
% 
\logar is an automaton over an alphabet in which each symbol is a
control location paired with a set of conditions on a DIFC store.
% 
\logar accepts sequences of conditions that represent an execution
of \authlog in which (1) \loginit completes execution (by executing
the operation at control location \cc{L4}, on which \logar
transitions from state $0$ to state $1$);
% 
(2) \logger is entered and memory has the rights to (a) read from
\msg, (b) read from \logfile, and (c) write to \logfile (by
executing the operation at control location \cc{L5}, on which
\logar transitions from state $1$ to state $2$);
% 
(3) \logger attempts (a) to read from \msg at \cc{L5} without
the right to read from \msg or (b) to append to \logfile at
\cc{L6} without either the right to read from or write to
\logfile (on which \logar transitions from state $2$ to state $3$).

% Walk through the non-interference policy.
A temporal non-interference policy defines undesired flows from a set
of source objects to sink objects.
% walk through policy for example:
The non-interference policy for \authlog can be expressed as an
automaton \logni over an alphabet in which each symbol is a
control location paired with a condition on a DIFC store.
% 
In particular, each condition is defined over predicates of the
form $\taint(\cc{X})$ that stores whether the program
execution may have influenced the value stored in the object
stored in object variable \cc{X}.
% 
As in existing work on DIFC languages~\cite{myers99,giffin12}, the
information stored in the taint predicate over-approximates
information about what objects may store different values over
different executions of a program.

% Describe the trace property expressed in the automaton.
In particular, \logni accepts sequences of conditions that
represent an execution of \authlog in which (1) \loginit completes
execution with \logfile untainted (by executing the operation at
control location \cc{L4}, on which \logni transitions from
state $0$ to state $1$);
% 
(2) \logni executes \logger an unbounded number of times with \msg
untainted (by executing the operation at control location
\cc{L0}, on which \logni transitions from state $1$ to state
$2$, and then transferring control to the environment, on which
\logni transitions from state $2$ to state $1$);
% 
(3) \logfile is tainted.

% Describe how we instrument \authlog.
\paragraph{Instrumenting \authlog}
% Describe the output.
The complete \authlog in \autoref{fig:authlog}, including label operations
highlighted in gray, satisfies the DIFC policy \logar and
non-interference policy \logni.
%
The instrumented \loginit creates a fresh category $c$ (\cc{LBL1a}),
and \loginit and \logger use $c$ to satisfy \logar and \logni.
%
In particular, when \loginit returns control to its environment, the
label of memory (chosen at \cc{LBL4b}) is higher at $c$ than the
label of the log file at $c$ (chosen at line \cc{LBL1b}), and the
declassification of memory (chosen at line \cc{LBL4a}) does not
contain $c$;
%
thus, no matter what label operations the environment executes,
including the operations in \malclient (\autoref{fig:authlog}), the
environment cannot write to the log file, ensuring that \logni remains
in state $2$.
%
However, because memory can read from an object with a lower label,
the environment can read from \logfile, which ensures that when
\logger exits, \logar transitions to state $1$, not to state $3$.
%
\BH{summarize the game construction in two lines}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../p"
%%% End: 
