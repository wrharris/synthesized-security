% sum up the FMCAD paper on sygus
\subsection{The SyGuS framework for program synthesis}
\label{sec:program-syn}
%
The problem of program synthesis is to take a declare specification of
correctness and automatically generate a program that satisfies the
specification.
%
Over the past decade, there has been a trend where the task of program
synthesis has been aided by use of a \emph{structure hypothesis} such
as a partial program sketch~\cite{solar-lezama05}, program
components~\cite{jha10,gulwani11}, and skeleton and example
behaviors~\cite{udupa13}.
%
In previous work, co-PI Seshia and collaborators unified these
seemingly-different approaches under a common underlying computational
problem termed as syntax-guided synthesis (SyGuS)~\cite{alur13}.
%
Given a function $f$ to be synthesized, a correctness specification for
$f$ given as a logical formula $\varphi$ (using symbols from a
background theory $T$), and a language of expressions $L$ (typically
specified as a grammar), the syntax-guided synthesis problem is to
find an implementation expression $e \in L$ such that the formula
$\varphi[ f / e ]$ is valid in the theory $T$.

% benchmarks
Since it was proposed in 2013 by PI Seshia and collaborators, the
SyGuS problem has become widely studied, with several researchers
working on both solution techniques and applications.
%
They have maintained an annual solver competition (similar to those
maintained for Boolean SAT and SMT solvers) since 2014, with growing
participation.

% give a running example:
\subsubsection{Solving SyGuS using constraint-based learning and verification}
\label{sec:sygus-ex}
%
As an example of the operation of one class of synthesizers developed
as part of SyGuS, consider the problem of synthesizing a program which
returns the maximum of two integer inputs.
% 
The specification of the desired program $\max$ is given by:
\begin{align*}
  \max(x, y) \geq x \land %
  \max(x, y) \geq y \land %
  (\max(x, y) = x \lor \max(x, y) = y)
\end{align*}
%
A grammar of useful programs, including multiple programs that
implement $\max$ includes expressions constructed from addition,
subtraction, comparison, conditional operators and the integer
constants $0$ and $1$.

\begin{table}[t]
\normalsize
\begin{floatrow}
  \centering
  \ttabbox
  {\caption{Program components from productions}%
    \label{tab:constraint-component-example}}
  {\begin{tabular}{| c | r @{\hskip3pt} l |}
      \hline
      \textbf{Production} & \multicolumn{2}{| c |}{\textbf{Component}} \\
      \hline
      \multirow{3}{*}{$E \rightarrow \ITE(B, E, E)$} & 
      Inputs: & $(i_1:B) (i_2,i_3:E)$ \\
      & Output: & $(o: E)$\\
      & Spec:& $o = \ITE(i_1, i_2, i_3)$\\
      \hline
      \multirow{3}{*}{$B \rightarrow E \leq E$} & 
      Inputs: & $(i_1, i_2:E)$\\
      & Output: & $(o:B)$\\
      & Spec: & $o=i_1\leq i_2$\\
      \hline
    \end{tabular}}
  %
  \ttabbox
  {\caption{A run of constraint-based learning}
    \label{tab:constraint-alg-trace}}
  {\begin{tabular*}{\linewidth}{| c | l | c |}\hlx{hv}
      \textbf{Iter} & 
      \textbf{Loop-free program} &
      \textbf{Counter-ex.} \tnl\hlx{vhv}
      1 & $o_1:=x$ & $\zug{x = -1, y = 0}$\tnl
      \hline
      \multirow{2}{*}{2} & $o_1:=x~\leq~x$ & \;\tnl
      & $o_2:=\ITE(o_1,~y,~x)$ & $\zug{x = 0, y = -1}$ \tnl
      \hline
      \multirow{2}{*}{3} &
      $o_1:= y \geq x$ & 
      \multirow{2}{*}{--} \tnl
      & $o_2 := \ITE(o_1, y, x)$ & \tnl
      \hline
    \end{tabular*}}
\end{floatrow}
\end{table}
%
Many solution methods are based on the iterative \emph{counter-example
guided inductive synthesis} strategy~\cite{solar-lezama06} that
combines a learning algorithm with a verification oracle.
%
The symbolic CEGIS approach uses a constraint solver both for
searching for a candidate expression that works for a set of concrete
input examples (concept learning) and verification of validity of an
expression for all possible inputs. We use component based synthesis
of loop-free programs as described by Jha et
al.~\cite{jha10,gulwani11}.
%
Each production in the grammar corresponds to a component in a
library.
%
A loop-free program comprising these components corresponds to an
expression from the grammar.
%
Some example components for the illustrative example are shown in
\autoref{tab:constraint-component-example}, along with their
corresponding productions.
%
The input/output ports of these components are typed and only
well-typed programs correspond to well-formed expressions from the
grammar.
%
To ensure this, Jha et al.'s encoding~\cite{gulwani11} is extended
with typing constraints.

% walk through a run of the algorithm
A run of the algorithm for learning a program that computes
$\mathrm{max}$ is given in \autoref{tab:constraint-alg-trace}.
%
The library of allowed components is instantiated to contain one
instance each of $\ITE$ and all comparison operators($\leq ,\geq ,=$)
and the concrete example set is initialized with $\zug{x=0, y=0}$. The
first candidate loop-free program synthesized corresponds to the
expression $x$.
%
This candidate is submitted to the verification oracle which returns
with $\zug{x=-1,y=0}$ as a counterexample.
%
This counterexample is added to the concrete example set and the
learning algorithm is queried again.
%
The SMT formula for learning a candidate expression is solved in an
incremental fashion; i.e., the constraint for every new example is
added to the list of constraints from the previous examples.
%
If synthesis fails for a component library, we add one instance of
every operator to the library and restart the algorithm with the new
library. 

%
In the proposed project, we plan to leverage SyGuS solvers
extensively, and create new solution techniques that are customized to
the class of security problems we target.

