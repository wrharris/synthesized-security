\subsubsection{Synthesizing Obfuscators for Privacy and Utility}
\label{sec:syn-obfs}
% high-level material:
Many systems transmit information to the outside world during their
operation.
%
For example, location-based services require devices such as
smartphones to transmit location information to other devices or to
servers in the cloud.
%
Similarly, in defense and aerospace applications, a network of drones
may need to broadcast location information to a variety of agents,
including other drones, ground personnel, and remote base
stations.
%
These settings often involve nodes that are resource-constrained or
connected in ad-hoc, dynamically-changing networks.
%
Some of the transmitted information may reveal secrets about the
system or its users; therefore, privacy is an important design
consideration.
%
At the same time, the agents to which this information is being sent
must have enough information to provide relevant services or perform
other actions.
%
Thus, the transmission of information from the system to the outside
world needs to balance the contrasting requirements of privacy and
utility.

\begin{figure*}
  \centering
  \begin{floatrow}
    % include grid of Alice and Bob stepping:
    \ffigbox[0.48\textwidth]{
      \includegraphics[width=0.5\linewidth]{fig/obfuscation-grid.pdf} }
    {\caption{Illustration of paths of length two taken by Alice and
        Bob that result in them inhabiting the same location.
        % 
        Alice and Bob's locations at step $n$ are labeled $t_n$ in red
        and blue, respectively. } 
      \label{fig:obf-grid} }

    % include policy:
    \ffigbox[0.48\textwidth]{
      \includegraphics[width=\linewidth]{fig/obf-plant.pdf} }
    {\caption{A state machine in which each state corresponds to a pair
        of locations inhabited by Alice and Bob and each edge is labeled
        by the pair of steps taken by Alice and Bob.
        % 
        Secret states, in which Alice and Bob inhabit the same location,
        are highlighted in red. } 
      \label{fig:obf-plant} }
  \end{floatrow}
\end{figure*}

% introduce a running example
As a motivating example, consider two users Alice and Bob who are
colocated in the same building.
%
Information about Alice and Bob's locations need to be sent to a
server and other agents in order to perform some useful actions, such
as adjusting the heating system based on their location or directing
them to the nearest coffee machine.
%
However, suppose that Alice and Bob want to keep particular facts
about the locations, e.g., if they are ever in the same room at the
same time, secret from other agents.
%
Alice and Bob wish to have a mechanism that allows them to provide
information about their locations to other agents that accurately
reflects their actual location, but keeps such facts secret.
%
For example, \autoref{fig:obf-plant} contains a graphical depiction of
Alice and Bob taking simultaneous trips, each consisting of two
steps.
%
A correct mechanism should reveal that Alice and Bob have locations
close to their actual locations at all points in time, but should not
reveal that they are both in the same room after two steps.

% context: an event generator:
In previous work~\cite{wu16}, we developed a solution for the above
problem in the following particular context.
%
Suppose that an ``event generator'' may be deployed (e.g., on Alice
and Bob's phone) that observes all events corresponding to Alice's
movements and may optionally edit them before broadcasting them to
other agents in the system.
%
Suppose further that the quality of the service that requires tracking
Alice's reported location degrades based on the Euclidean distance
from her true location.
%
The problem that we addressed to design an event generator that
generates an output event stream that does not reveal whether Alice
visited a secret location while also providing sufficient accuracy for
determining her location for the relevant services.

% solution at a high-level
Our key solution uses an automata-theoretic approach.
%
In particular, we model the composition of users and the applications
that collect and reveal their information as a state machine, with
designated secret states;
%
a correct mechanism must not reveal a system is currently in a secret
state.
%
A state machine modeling the possible simultaneous locations of Alice
and Bob, with secret states in which the are colocated in the same
room, is given in \autoref{fig:obf-plant}.
%
We cast the problem of synthesizing a mechanism that reveals useful
but secure information about its agents as synthesizing an \emph{edit
  function} that maintains its own internal state, which it consults
in combination with the next action taken by its monitored system to
determine what potentially-altered information to reveal to other
agents.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../p"
%%% End: 
